-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sugarcrm_dev
-- ------------------------------------------------------
-- Server version	5.5.38-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `account_type` varchar(50) DEFAULT NULL,
  `industry` varchar(50) DEFAULT NULL,
  `annual_revenue` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `rating` varchar(100) DEFAULT NULL,
  `phone_office` varchar(100) DEFAULT NULL,
  `phone_alternate` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `ownership` varchar(100) DEFAULT NULL,
  `employees` varchar(10) DEFAULT NULL,
  `ticker_symbol` varchar(10) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `sic_code` varchar(10) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_accnt_id_del` (`id`,`deleted`),
  KEY `idx_accnt_name_del` (`name`,`deleted`),
  KEY `idx_accnt_assigned_del` (`deleted`,`assigned_user_id`),
  KEY `idx_accnt_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_audit`
--

DROP TABLE IF EXISTS `accounts_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_accounts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_audit`
--

LOCK TABLES `accounts_audit` WRITE;
/*!40000 ALTER TABLE `accounts_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_bugs`
--

DROP TABLE IF EXISTS `accounts_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_bugs` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_bug_acc` (`account_id`),
  KEY `idx_acc_bug_bug` (`bug_id`),
  KEY `idx_account_bug` (`account_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_bugs`
--

LOCK TABLES `accounts_bugs` WRITE;
/*!40000 ALTER TABLE `accounts_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_cases`
--

DROP TABLE IF EXISTS `accounts_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_cases` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_case_acc` (`account_id`),
  KEY `idx_acc_acc_case` (`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_cases`
--

LOCK TABLES `accounts_cases` WRITE;
/*!40000 ALTER TABLE `accounts_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_contacts`
--

DROP TABLE IF EXISTS `accounts_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_contact` (`account_id`,`contact_id`),
  KEY `idx_contid_del_accid` (`contact_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_contacts`
--

LOCK TABLES `accounts_contacts` WRITE;
/*!40000 ALTER TABLE `accounts_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_opportunities`
--

DROP TABLE IF EXISTS `accounts_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_opportunity` (`account_id`,`opportunity_id`),
  KEY `idx_oppid_del_accid` (`opportunity_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_opportunities`
--

LOCK TABLES `accounts_opportunities` WRITE;
/*!40000 ALTER TABLE `accounts_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_actions`
--

DROP TABLE IF EXISTS `acl_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_actions` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `acltype` varchar(100) DEFAULT NULL,
  `aclaccess` int(3) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclaction_id_del` (`id`,`deleted`),
  KEY `idx_category_name` (`category`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_actions`
--

LOCK TABLES `acl_actions` WRITE;
/*!40000 ALTER TABLE `acl_actions` DISABLE KEYS */;
INSERT INTO `acl_actions` VALUES ('121c7305-b0c5-1ada-3b7d-5448ff3bca0e','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Leads','module',90,0),('147cec9b-b5b4-6869-52a8-544900ae803f','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Documents','module',90,0),('161c7761-7343-c805-5bb7-5448ffbbfb80','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','EmailTemplates','module',89,0),('16f84630-fd7f-fc36-9677-5448ff080f57','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Cases','module',90,0),('1c5521e0-7296-df7a-a52c-5448ff1dc923','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Leads','module',90,0),('1d177b9d-5431-5eec-6051-5448ff600421','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Cases','module',90,0),('1d4028b2-3f7c-1585-4a44-5448ff2d8691','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','EmailTemplates','module',90,0),('21bf0953-37be-dc38-def0-54490086bdc9','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Documents','module',90,0),('224e0d05-16be-7e68-062e-544900661de2','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Documents','module',90,0),('243e4bea-2b3e-a873-18d5-5448ff18572f','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Leads','module',90,0),('245f36ea-f4cf-b455-6740-5448ffbcf417','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','EmailTemplates','module',90,0),('246d4c4f-6489-ebcb-0d8f-5449002e32fb','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Meetings','module',89,0),('2a9a3dfd-4bc9-30ff-d8c3-5448ff20a559','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Leads','module',90,0),('2b67a6cf-8010-c693-2804-5449007c81d8','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Meetings','module',90,0),('2b8b6104-09eb-e379-3dca-5448ff4c5373','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','EmailTemplates','module',90,0),('2d8d55a6-d5a3-bebd-5c52-5448ff97c39a','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','ProspectLists','module',89,0),('30b4e812-b68f-24bb-e7bf-5448ffc16897','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Leads','module',90,0),('32421135-fb5e-9e17-978e-5448ff7b404a','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Contacts','module',90,0),('32a30851-a206-e3b5-c7f0-544900231ffc','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Tasks','module',90,0),('32a33a87-5671-0c49-db01-5449006f226b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Meetings','module',90,0),('32a6cd33-f272-b143-f956-5448ff168a18','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','EmailTemplates','module',90,0),('35afe513-8a33-3ee5-13fc-5448fff0766c','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','ProspectLists','module',90,0),('3acf844e-0e80-c194-564b-54490036e1dc','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Meetings','module',90,0),('3b37816d-30f0-10ed-7eb5-5448ff48611d','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Notes','module',90,0),('3fdbf3f8-53d9-ef9d-3b5f-5448ff859186','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','ProspectLists','module',90,0),('411f645c-16e3-1119-b48a-5448ffaf43ba','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Accounts','module',89,0),('41dad169-2565-7769-1eaf-5448ffbfc4ff','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','EmailTemplates','module',90,0),('444e4a4e-56b8-50e7-726b-5448ff82ced1','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Notes','module',90,0),('450243dd-344d-8724-7d22-5449007e7ba9','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Meetings','module',90,0),('451cb4b9-f033-5885-475d-5448ff86abb4','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Leads','module',90,0),('456ec426-b199-bb01-49be-544900a40b2a','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Documents','module',90,0),('485e34e2-4a72-ab1d-6fb6-5448ffda68cf','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Accounts','module',90,0),('4a6df86c-0b4d-b840-466e-5448ff57303e','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Notes','module',90,0),('4c02266a-c6b1-35b5-a3ee-544900eed990','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Meetings','module',90,0),('4c17f34d-43b2-e1d8-e087-5448ff773f72','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','ProspectLists','module',90,0),('508f2e9f-55e7-b686-2dcd-5448ffc2eaac','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Notes','module',90,0),('51689937-3c80-a783-a455-5448ff5eb419','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Accounts','module',90,0),('5234c3bc-1399-c050-998e-5448ffbb004e','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','ProspectLists','module',90,0),('525662aa-86e4-39a4-6cd7-5448ff59c3c5','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','EmailTemplates','module',90,0),('52c2fb0c-e279-5c03-9410-5448ff1acc05','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Contacts','module',90,0),('54ba20eb-229e-5407-2e1c-544900eb6616','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Emails','module',89,0),('56976b13-acbe-c92b-fbf1-5448ff4a1065','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','EmailMarketing','module',90,0),('58533229-a6b9-3ac6-414a-5448ffcc563b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','ProspectLists','module',90,0),('58b28b8c-17a3-e416-3f68-5448fffd0fbf','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Accounts','module',90,0),('5b8a0e36-c6a0-1d61-efc3-5448ffc91f1f','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Leads','module',89,0),('5e7596a0-b415-488d-0733-5448ff6d2b23','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','ProspectLists','module',90,0),('5e7e32d5-0d48-cfee-5ed9-5448ff70a006','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Contacts','module',90,0),('5f821518-9792-ebdd-43dd-544900a6922b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Emails','module',90,0),('5fb06f4e-85cd-9171-5362-5448ff9f356f','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Accounts','module',90,0),('61b10c40-261b-e8af-d1db-5448ff977dbd','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Users','module',89,0),('6494678f-8a2a-a172-5761-5448ffd3dadd','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','ProspectLists','module',90,0),('64c5908e-1479-988c-3953-5448ffb221fa','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Opportunities','module',89,0),('65a4a720-2c47-c562-15db-5449005a0833','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Emails','module',90,0),('6992acd7-d51a-fca2-3c43-5448ffbf985f','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','EmailMarketing','module',90,0),('6afeeecf-9798-20a9-3073-5448ff224acf','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Contacts','module',90,0),('6bd3ff19-6b51-3c2a-e375-5448ffb1ebfe','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Users','module',90,0),('6ceba986-693b-925c-e6a7-5448ffbf2f45','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Opportunities','module',90,0),('6d1957af-5879-ead5-8e9f-5448ff6edcc3','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Notes','module',90,0),('6ff51e91-e1b0-df0b-199c-5448ff034ae9','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Campaigns','module',89,0),('72879dae-64a2-bce3-a9b6-5448ff75549b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Contacts','module',90,0),('730975b7-fa0f-4c75-ee5b-5448ff5fa1e5','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Opportunities','module',90,0),('76108422-ec17-e24e-ee90-5448fffeaa42','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Campaigns','module',90,0),('79298c44-2b42-0da1-f5b7-5448ffd6d9be','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Opportunities','module',90,0),('798cbbab-121a-6363-2fde-544900ea2f60','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Emails','module',90,0),('7a1d3f28-f874-723b-4ef7-5448ffecc214','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Users','module',90,0),('7b109565-975e-59d7-49dd-5448ff370451','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','EmailMarketing','module',90,0),('7db9688f-2cd9-d854-28bc-5448ffd2833d','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Contacts','module',90,0),('7f4d702e-634f-c4b6-374c-5448ff4062d2','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Opportunities','module',90,0),('80214842-7211-592e-558f-544900798870','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Emails','module',90,0),('81c0c3f7-6539-b852-83ff-5448ff645c70','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Campaigns','module',90,0),('84dfecbf-173b-7774-2cf1-5448ff20e638','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Contacts','module',90,0),('856d28dc-4cf7-98d3-fc5f-5448ff2aa107','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Opportunities','module',90,0),('86d1aa59-6069-a9e8-1c3b-5448ff33aba1','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Prospects','module',89,0),('87aa6d14-77a7-5a89-fbae-544900fad587','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Emails','module',90,0),('880b7dc4-b154-f918-c182-5448ff4f49ba','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','EmailMarketing','module',89,0),('89182c5b-62ce-55c4-c5bb-5448ff77ea1b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Bugs','module',89,0),('898eddca-e779-8755-c1e0-5448ffe761e7','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','ProjectTask','module',89,0),('89f2c796-5705-a1fa-2b8c-5448ffb26cee','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','EmailMarketing','module',90,0),('89f3b8f3-e1db-ac52-95af-5448ff5efde2','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Campaigns','module',90,0),('8a49cfb5-0d04-82b3-1f68-5449004ceaba','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Meetings','module',90,0),('8b8c1616-06e1-8ea7-74da-5448ffd3fabc','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Opportunities','module',90,0),('8b9001a1-5ebb-b604-e36a-5448ff7de5ac','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Cases','module',90,0),('8ceca249-18cf-23ab-4bad-5448ff8bc2b6','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Prospects','module',90,0),('8dcbcf28-00d8-fca3-acbf-5449007fbd87','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Emails','module',90,0),('8dde0a98-120b-0425-2cbd-5449009dfe87','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Documents','module',90,0),('8f3b0f55-7026-7646-0c7b-5448ff3d6d22','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Project','module',90,0),('8f7951a5-afb2-7e73-7a1a-5448ff881db8','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','EmailMarketing','module',90,0),('8f89d77e-54d4-14a9-6012-5448ff38ce5c','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Bugs','module',90,0),('8fae7d40-fb83-c4d0-47a8-5448ff2b1cd1','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','ProjectTask','module',90,0),('909d2c4b-8395-edea-3aa7-5448ff7f9070','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Campaigns','module',90,0),('91ecabe2-379c-2f99-0601-5448ff91188a','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','EmailTemplates','module',90,0),('9311f235-ef7f-ad0d-6d98-5448ff96c38c','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Prospects','module',90,0),('93c09300-8092-0b1a-8d67-5448ff3b42f4','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Opportunities','module',90,0),('9479cfa2-5167-df23-40bd-544900611dc6','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Meetings','module',90,0),('94990941-66cd-1668-aefa-54490067f063','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Tasks','module',90,0),('95aeecfc-cb76-5aa8-646d-5448ff0b1672','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Bugs','module',90,0),('95cc1792-7ae4-ac66-e8be-5448ffc8cc2b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','ProjectTask','module',90,0),('96c2d6a6-667b-cadf-11b2-5448ffb0ba21','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Campaigns','module',90,0),('980637a1-c328-7b8e-0044-544900255231','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Emails','module',90,0),('98559577-0652-26f4-3a03-5448ffde8997','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','EmailMarketing','module',90,0),('993840d0-9378-6bb3-09e0-5448ff0e7c19','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Prospects','module',90,0),('9ce9cf65-ec87-a6fe-24fe-5448ffbbc297','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Campaigns','module',90,0),('9f572f62-00bc-0eec-48e0-5448ff75bc64','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Prospects','module',90,0),('9f8a23cc-fb4e-2889-6d9d-5448ffee9e97','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Bugs','module',90,0),('a2ff9c1b-ab2e-a58e-b7a7-5448ff6e7efe','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Campaigns','module',90,0),('a604f995-c58b-2b99-e75b-5448ffdd56c0','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Bugs','module',90,0),('a6155d78-e35c-3d0a-21f1-5448ff346326','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Prospects','module',90,0),('a6d1209a-92de-e002-8334-5448ffc09070','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Users','module',90,0),('ab84501f-e507-fa87-342f-54490057066a','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','EAPM','module',89,0),('ac1d0578-8621-f8ca-b0c9-5448ff24fd5e','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Cases','module',90,0),('add4749e-0a64-ca30-9b63-5448ffa31028','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Bugs','module',90,0),('b36a6114-4701-5088-0411-54490042344f','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','EAPM','module',90,0),('b3e305f2-1655-2cb8-1e8d-5448ff2cea02','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Accounts','module',90,0),('b43ae565-6153-34a4-8fd5-5448ffc29241','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Bugs','module',90,0),('ba0d384b-93d2-6331-de72-54490087b1e4','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','EAPM','module',90,0),('ba5706ee-ebbe-6498-41c0-5448ffd465e7','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Bugs','module',90,0),('bbe420ff-6312-7b3a-c1fc-5448ff76ce3c','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Project','module',89,0),('bf5e3170-cf49-17d1-d4d0-5448ff0a9166','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Users','module',90,0),('bf65bc94-4a65-e82d-97f0-5448ff9e0fca','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Accounts','module',90,0),('c0210fac-da11-5f1a-3a25-5448ffc2e266','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Leads','module',90,0),('c0d1ab2b-7a64-a73d-7316-5448ff4b2eb7','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Calls','module',89,0),('c1948020-2d70-4398-f1e7-544900b166f0','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Documents','module',90,0),('c1f6ce30-c5d3-c1ad-b8dd-5448ff3098a3','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Prospects','module',90,0),('c23519d8-38bb-918c-0de6-5449002a2786','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','EAPM','module',90,0),('c2b7be7e-616e-df68-f0a2-5448ff04eeb6','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','ProjectTask','module',90,0),('c3fe3f0f-11ec-d7a0-206f-5448ff4f3cdb','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Project','module',90,0),('c585fdd9-2308-8e93-399b-5448ffb867cd','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Accounts','module',90,0),('c6e64c7d-86aa-0a45-2e48-5448ff2c13c0','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Calls','module',90,0),('c77afd09-56a0-7fcd-db8b-5448ff90d371','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Users','module',90,0),('c85024b6-371c-13a7-4b38-54490072a046','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','EAPM','module',90,0),('cc272e1a-5334-9bec-8206-5448ff716fb7','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Project','module',90,0),('cc3630ad-159b-3d0e-c228-5448ff455cb8','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','ProjectTask','module',90,0),('cd041f75-ccff-b483-29ba-5448ffbdc82c','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Calls','module',90,0),('ce25a29b-1251-7d73-4fba-5448ff81a37d','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Prospects','module',90,0),('ce660741-30aa-4b48-be71-54490084adcb','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','EAPM','module',90,0),('cfa027fc-175c-6a26-dc22-5448ff0a3ffb','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Users','module',90,0),('d12302f7-f67a-3e57-db99-544900ea1f96','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Documents','module',90,0),('d2d3ae28-2d9d-def5-eda3-5448ff154997','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','ProjectTask','module',90,0),('d498d423-3ee2-56ab-4170-544900e08829','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','EAPM','module',90,0),('d529f796-6119-9798-a2c7-5448ffb668a8','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Calls','module',90,0),('d5c114ec-28e8-4086-8ef5-5448ff1017bf','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Users','module',90,0),('d5e8c1de-7677-70bf-55b3-5448ff87581b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Project','module',90,0),('d6b92656-86a5-0efa-f197-544900a0bda9','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Tasks','module',89,0),('d8314829-0895-a203-f812-5448ff663f55','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Cases','module',89,0),('d8f6cf19-bba4-9df7-e4d1-5448ff1cc479','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','ProjectTask','module',90,0),('daa2c785-9a74-822c-27eb-544900b52b5c','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','EAPM','module',90,0),('db47d970-04a0-8a1b-2986-5448ff89b1c5','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Calls','module',90,0),('dc76b6f0-e1eb-2b9f-b9f1-5448ffdba62c','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Project','module',90,0),('dcc854da-893f-17f6-47c9-54490072f188','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Tasks','module',90,0),('df33f23d-3cb6-b6b7-7f53-5448ffaeda4b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','EmailMarketing','module',90,0),('e04938b1-f943-9df1-d7d4-5448ff315fe3','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Cases','module',90,0),('e0f8feec-782e-6d01-441e-5448ff9544ce','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Notes','module',89,0),('e11d7676-ada8-32f6-7235-5448ff3dc2ad','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','ProjectTask','module',90,0),('e16995b0-24ec-6a3b-6038-5448ff42a373','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Calls','module',90,0),('e4ec9aac-f043-1c2c-d8ab-5449007a9584','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Tasks','module',90,0),('e6315296-65fc-da12-c99e-5448ff679212','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','import','Project','module',90,0),('e7073d2b-2935-c9d5-2570-5448ffd5bfc6','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','view','Notes','module',90,0),('e788bcf5-5296-9bcf-e72b-5448ff88df3b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Calls','module',90,0),('e86ea245-9ade-2e88-1eea-5448ffda74eb','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Cases','module',90,0),('eb13396f-13af-170d-f478-54490064ab08','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Tasks','module',90,0),('ecc4b163-2533-28a7-bc3a-5448ffce3f73','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','export','Project','module',90,0),('eda96d1d-db9b-0978-1381-5448ffae5352','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Calls','module',90,0),('ee8b28e8-7840-51e8-46ab-544900073a08','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Documents','module',89,0),('ee91f11c-1aa5-97d9-262f-5448ff55108f','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','edit','Cases','module',90,0),('ef630817-acbb-4d75-dcd8-5448ffa0fde5','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','access','Contacts','module',89,0),('f03a8405-174e-db0d-d350-5448ffc3a23c','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','list','Notes','module',90,0),('f1304243-f587-ef0c-5207-5449007c7bcb','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','delete','Tasks','module',90,0),('f6a40c33-1f19-4cc1-6933-5449009ce614','2014-10-23 13:17:35','2014-10-23 13:17:35','1','','massupdate','Tasks','module',90,0);
/*!40000 ALTER TABLE `acl_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles`
--

DROP TABLE IF EXISTS `acl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles`
--

LOCK TABLES `acl_roles` WRITE;
/*!40000 ALTER TABLE `acl_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles_actions`
--

DROP TABLE IF EXISTS `acl_roles_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles_actions` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `action_id` varchar(36) DEFAULT NULL,
  `access_override` int(3) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acl_role_id` (`role_id`),
  KEY `idx_acl_action_id` (`action_id`),
  KEY `idx_aclrole_action` (`role_id`,`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles_actions`
--

LOCK TABLES `acl_roles_actions` WRITE;
/*!40000 ALTER TABLE `acl_roles_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_roles_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles_users`
--

DROP TABLE IF EXISTS `acl_roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id` (`role_id`),
  KEY `idx_acluser_id` (`user_id`),
  KEY `idx_aclrole_user` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles_users`
--

LOCK TABLES `acl_roles_users` WRITE;
/*!40000 ALTER TABLE `acl_roles_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_book`
--

DROP TABLE IF EXISTS `address_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_book` (
  `assigned_user_id` char(36) NOT NULL,
  `bean` varchar(50) DEFAULT NULL,
  `bean_id` char(36) NOT NULL,
  KEY `ab_user_bean_idx` (`assigned_user_id`,`bean`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_book`
--

LOCK TABLES `address_book` WRITE;
/*!40000 ALTER TABLE `address_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs`
--

DROP TABLE IF EXISTS `bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `bug_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` varchar(255) DEFAULT NULL,
  `work_log` text,
  `found_in_release` varchar(255) DEFAULT NULL,
  `fixed_in_release` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `product_category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bugsnumk` (`bug_number`),
  KEY `bug_number` (`bug_number`),
  KEY `idx_bug_name` (`name`),
  KEY `idx_bugs_assigned_user` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs`
--

LOCK TABLES `bugs` WRITE;
/*!40000 ALTER TABLE `bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs_audit`
--

DROP TABLE IF EXISTS `bugs_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_bugs_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs_audit`
--

LOCK TABLES `bugs_audit` WRITE;
/*!40000 ALTER TABLE `bugs_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls`
--

DROP TABLE IF EXISTS `calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(2) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `direction` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_call_name` (`name`),
  KEY `idx_status` (`status`),
  KEY `idx_calls_date_start` (`date_start`),
  KEY `idx_calls_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_calls_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls`
--

LOCK TABLES `calls` WRITE;
/*!40000 ALTER TABLE `calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_contacts`
--

DROP TABLE IF EXISTS `calls_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_contacts` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_call_call` (`call_id`),
  KEY `idx_con_call_con` (`contact_id`),
  KEY `idx_call_contact` (`call_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_contacts`
--

LOCK TABLES `calls_contacts` WRITE;
/*!40000 ALTER TABLE `calls_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_leads`
--

DROP TABLE IF EXISTS `calls_leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_leads` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_call_call` (`call_id`),
  KEY `idx_lead_call_lead` (`lead_id`),
  KEY `idx_call_lead` (`call_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_leads`
--

LOCK TABLES `calls_leads` WRITE;
/*!40000 ALTER TABLE `calls_leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_users`
--

DROP TABLE IF EXISTS `calls_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_users` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_call_call` (`call_id`),
  KEY `idx_usr_call_usr` (`user_id`),
  KEY `idx_call_users` (`call_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_users`
--

LOCK TABLES `calls_users` WRITE;
/*!40000 ALTER TABLE `calls_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_log`
--

DROP TABLE IF EXISTS `campaign_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_log` (
  `id` char(36) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `target_tracker_key` varchar(36) DEFAULT NULL,
  `target_id` varchar(36) DEFAULT NULL,
  `target_type` varchar(100) DEFAULT NULL,
  `activity_type` varchar(100) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `list_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `more_information` varchar(100) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_camp_tracker` (`target_tracker_key`),
  KEY `idx_camp_campaign_id` (`campaign_id`),
  KEY `idx_camp_more_info` (`more_information`),
  KEY `idx_target_id` (`target_id`),
  KEY `idx_target_id_deleted` (`target_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_log`
--

LOCK TABLES `campaign_log` WRITE;
/*!40000 ALTER TABLE `campaign_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_trkrs`
--

DROP TABLE IF EXISTS `campaign_trkrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_trkrs` (
  `id` char(36) NOT NULL,
  `tracker_name` varchar(30) DEFAULT NULL,
  `tracker_url` varchar(255) DEFAULT 'http://',
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `is_optout` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `campaign_tracker_key_idx` (`tracker_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_trkrs`
--

LOCK TABLES `campaign_trkrs` WRITE;
/*!40000 ALTER TABLE `campaign_trkrs` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_trkrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigns`
--

DROP TABLE IF EXISTS `campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `tracker_count` int(11) DEFAULT '0',
  `refer_url` varchar(255) DEFAULT 'http://',
  `tracker_text` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `impressions` int(11) DEFAULT '0',
  `currency_id` char(36) DEFAULT NULL,
  `budget` double DEFAULT NULL,
  `expected_cost` double DEFAULT NULL,
  `actual_cost` double DEFAULT NULL,
  `expected_revenue` double DEFAULT NULL,
  `campaign_type` varchar(100) DEFAULT NULL,
  `objective` text,
  `content` text,
  `frequency` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `camp_auto_tracker_key` (`tracker_key`),
  KEY `idx_campaign_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigns`
--

LOCK TABLES `campaigns` WRITE;
/*!40000 ALTER TABLE `campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigns_audit`
--

DROP TABLE IF EXISTS `campaigns_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_campaigns_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigns_audit`
--

LOCK TABLES `campaigns_audit` WRITE;
/*!40000 ALTER TABLE `campaigns_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` text,
  `work_log` text,
  `account_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `casesnumk` (`case_number`),
  KEY `case_number` (`case_number`),
  KEY `idx_case_name` (`name`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_cases_stat_del` (`assigned_user_id`,`status`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases`
--

LOCK TABLES `cases` WRITE;
/*!40000 ALTER TABLE `cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases_audit`
--

DROP TABLE IF EXISTS `cases_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_cases_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_audit`
--

LOCK TABLES `cases_audit` WRITE;
/*!40000 ALTER TABLE `cases_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases_bugs`
--

DROP TABLE IF EXISTS `cases_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_bugs` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_cas_bug_cas` (`case_id`),
  KEY `idx_cas_bug_bug` (`bug_id`),
  KEY `idx_case_bug` (`case_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_bugs`
--

LOCK TABLES `cases_bugs` WRITE;
/*!40000 ALTER TABLE `cases_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `category` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `value` text,
  KEY `idx_config_cat` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES ('notify','fromaddress','do_not_reply@example.com'),('notify','fromname','SugarCRM'),('notify','send_by_default','1'),('notify','on','1'),('notify','send_from_assigning_user','0'),('info','sugar_version','6.5.18'),('MySettings','tab','YToxNjp7aTowO3M6NDoiSG9tZSI7aToxO3M6ODoiQWNjb3VudHMiO2k6MjtzOjg6IkNvbnRhY3RzIjtpOjM7czoxMzoiT3Bwb3J0dW5pdGllcyI7aTo0O3M6NToiTGVhZHMiO2k6NTtzOjg6IkNhbGVuZGFyIjtpOjY7czo5OiJEb2N1bWVudHMiO2k6NztzOjY6IkVtYWlscyI7aTo4O3M6OToiQ2FtcGFpZ25zIjtpOjk7czo1OiJDYWxscyI7aToxMDtzOjg6Ik1lZXRpbmdzIjtpOjExO3M6NToiVGFza3MiO2k6MTI7czo1OiJOb3RlcyI7aToxMztzOjU6IkNhc2VzIjtpOjE0O3M6OToiUHJvc3BlY3RzIjtpOjE1O3M6MTM6IlByb3NwZWN0TGlzdHMiO30='),('portal','on','0'),('tracker','Tracker','1'),('system','skypeout_on','1'),('sugarfeed','enabled','1'),('sugarfeed','module_UserFeed','1'),('sugarfeed','module_Leads','1'),('sugarfeed','module_Contacts','1'),('sugarfeed','module_Opportunities','1'),('sugarfeed','module_Cases','1'),('Update','CheckUpdates','automatic'),('system','name','SugarCRM_dev'),('license','msg_admin',''),('license','msg_all',''),('license','last_validation','success'),('license','latest_versions','YToxOntpOjA7YToyOntzOjc6InZlcnNpb24iO3M6NjoiNi41LjE2IjtzOjExOiJkZXNjcmlwdGlvbiI7czoxNjM6IlRoZSBsYXRlc3QgdmVyc2lvbiBvZiBTdWdhckNSTSBpcyA2LjUuMTYuICBQbGVhc2UgdmlzaXQgPGEgaHJlZj0iaHR0cDovL3N1cHBvcnQuc3VnYXJjcm0uY29tIiB0YXJnZXQ9Il9uZXciPnN1cHBvcnQuc3VnYXJjcm0uY29tPC9hPiB0byBhY3F1aXJlIHRoZSBsYXRlc3QgdmVyc2lvbi4iO319'),('Update','last_check_date','1414070327'),('system','adminwizard','1'),('notify','allow_default_outbound','2');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `lead_source` varchar(255) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cont_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_contacts_del_last` (`deleted`,`last_name`),
  KEY `idx_cont_del_reports` (`deleted`,`reports_to_id`,`last_name`),
  KEY `idx_reports_to_id` (`reports_to_id`),
  KEY `idx_del_id_user` (`deleted`,`id`,`assigned_user_id`),
  KEY `idx_cont_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_audit`
--

DROP TABLE IF EXISTS `contacts_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_contacts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_audit`
--

LOCK TABLES `contacts_audit` WRITE;
/*!40000 ALTER TABLE `contacts_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_bugs`
--

DROP TABLE IF EXISTS `contacts_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_bugs` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_bug_con` (`contact_id`),
  KEY `idx_con_bug_bug` (`bug_id`),
  KEY `idx_contact_bug` (`contact_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_bugs`
--

LOCK TABLES `contacts_bugs` WRITE;
/*!40000 ALTER TABLE `contacts_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_cases`
--

DROP TABLE IF EXISTS `contacts_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_cases` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_case_con` (`contact_id`),
  KEY `idx_con_case_case` (`case_id`),
  KEY `idx_contacts_cases` (`contact_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_cases`
--

LOCK TABLES `contacts_cases` WRITE;
/*!40000 ALTER TABLE `contacts_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_users`
--

DROP TABLE IF EXISTS `contacts_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_users` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_users_con` (`contact_id`),
  KEY `idx_con_users_user` (`user_id`),
  KEY `idx_contacts_users` (`contact_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_users`
--

LOCK TABLES `contacts_users` WRITE;
/*!40000 ALTER TABLE `contacts_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cron_remove_documents`
--

DROP TABLE IF EXISTS `cron_remove_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cron_remove_documents` (
  `id` varchar(36) NOT NULL,
  `bean_id` varchar(36) DEFAULT NULL,
  `module` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cron_remove_document_bean_id` (`bean_id`),
  KEY `idx_cron_remove_document_stamp` (`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cron_remove_documents`
--

LOCK TABLES `cron_remove_documents` WRITE;
/*!40000 ALTER TABLE `cron_remove_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `cron_remove_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` char(36) NOT NULL,
  `name` varchar(36) DEFAULT NULL,
  `symbol` varchar(36) DEFAULT NULL,
  `iso4217` varchar(3) DEFAULT NULL,
  `conversion_rate` double DEFAULT '0',
  `status` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_currency_name` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields`
--

DROP TABLE IF EXISTS `custom_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_fields` (
  `bean_id` varchar(36) DEFAULT NULL,
  `set_num` int(11) DEFAULT '0',
  `field0` varchar(255) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `field9` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_beanid_set_num` (`bean_id`,`set_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields`
--

LOCK TABLES `custom_fields` WRITE;
/*!40000 ALTER TABLE `custom_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_revisions`
--

DROP TABLE IF EXISTS `document_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_revisions` (
  `id` varchar(36) NOT NULL,
  `change_log` varchar(255) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT NULL,
  `doc_url` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `revision` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documentrevision_mimetype` (`file_mime_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_revisions`
--

LOCK TABLES `document_revisions` WRITE;
/*!40000 ALTER TABLE `document_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT 'Sugar',
  `doc_url` varchar(255) DEFAULT NULL,
  `active_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `subcategory_id` varchar(100) DEFAULT NULL,
  `status_id` varchar(100) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `related_doc_id` char(36) DEFAULT NULL,
  `related_doc_rev_id` char(36) DEFAULT NULL,
  `is_template` tinyint(1) DEFAULT '0',
  `template_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_doc_cat` (`category_id`,`subcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_accounts`
--

DROP TABLE IF EXISTS `documents_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_accounts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_accounts_account_id` (`account_id`,`document_id`),
  KEY `documents_accounts_document_id` (`document_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_accounts`
--

LOCK TABLES `documents_accounts` WRITE;
/*!40000 ALTER TABLE `documents_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_bugs`
--

DROP TABLE IF EXISTS `documents_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_bugs` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_bugs_bug_id` (`bug_id`,`document_id`),
  KEY `documents_bugs_document_id` (`document_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_bugs`
--

LOCK TABLES `documents_bugs` WRITE;
/*!40000 ALTER TABLE `documents_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_cases`
--

DROP TABLE IF EXISTS `documents_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_cases` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_cases_case_id` (`case_id`,`document_id`),
  KEY `documents_cases_document_id` (`document_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_cases`
--

LOCK TABLES `documents_cases` WRITE;
/*!40000 ALTER TABLE `documents_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_contacts`
--

DROP TABLE IF EXISTS `documents_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_contacts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_contacts_contact_id` (`contact_id`,`document_id`),
  KEY `documents_contacts_document_id` (`document_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_contacts`
--

LOCK TABLES `documents_contacts` WRITE;
/*!40000 ALTER TABLE `documents_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_opportunities`
--

DROP TABLE IF EXISTS `documents_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_opportunities` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_docu_opps_oppo_id` (`opportunity_id`,`document_id`),
  KEY `idx_docu_oppo_docu_id` (`document_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_opportunities`
--

LOCK TABLES `documents_opportunities` WRITE;
/*!40000 ALTER TABLE `documents_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eapm`
--

DROP TABLE IF EXISTS `eapm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eapm` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `application` varchar(100) DEFAULT 'webex',
  `api_data` text,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `oauth_token` varchar(255) DEFAULT NULL,
  `oauth_secret` varchar(255) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_app_active` (`assigned_user_id`,`application`,`validated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eapm`
--

LOCK TABLES `eapm` WRITE;
/*!40000 ALTER TABLE `eapm` DISABLE KEYS */;
/*!40000 ALTER TABLE `eapm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addr_bean_rel`
--

DROP TABLE IF EXISTS `email_addr_bean_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addr_bean_rel` (
  `id` char(36) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `bean_id` char(36) NOT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `primary_address` tinyint(1) DEFAULT '0',
  `reply_to_address` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_email_address_id` (`email_address_id`),
  KEY `idx_bean_id` (`bean_id`,`bean_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addr_bean_rel`
--

LOCK TABLES `email_addr_bean_rel` WRITE;
/*!40000 ALTER TABLE `email_addr_bean_rel` DISABLE KEYS */;
INSERT INTO `email_addr_bean_rel` VALUES ('df384649-b1b6-2e14-2666-54490290c6ef','df4b953c-936b-b4c9-f71d-54490257cf94','1','Users',1,0,'2014-10-23 13:28:04','2014-10-23 13:28:04',0);
/*!40000 ALTER TABLE `email_addr_bean_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addresses`
--

DROP TABLE IF EXISTS `email_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addresses` (
  `id` char(36) NOT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `email_address_caps` varchar(255) DEFAULT NULL,
  `invalid_email` tinyint(1) DEFAULT '0',
  `opt_out` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ea_caps_opt_out_invalid` (`email_address_caps`,`opt_out`,`invalid_email`),
  KEY `idx_ea_opt_out_invalid` (`email_address`,`opt_out`,`invalid_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addresses`
--

LOCK TABLES `email_addresses` WRITE;
/*!40000 ALTER TABLE `email_addresses` DISABLE KEYS */;
INSERT INTO `email_addresses` VALUES ('df4b953c-936b-b4c9-f71d-54490257cf94','janis@itrisinajumi.lv','JANIS@ITRISINAJUMI.LV',0,0,'2014-10-23 13:28:04','2014-10-23 13:28:04',0);
/*!40000 ALTER TABLE `email_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_cache`
--

DROP TABLE IF EXISTS `email_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_cache` (
  `ie_id` char(36) DEFAULT NULL,
  `mbox` varchar(60) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `fromaddr` varchar(100) DEFAULT NULL,
  `toaddr` varchar(255) DEFAULT NULL,
  `senddate` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `mailsize` int(10) unsigned DEFAULT NULL,
  `imap_uid` int(10) unsigned DEFAULT NULL,
  `msgno` int(10) unsigned DEFAULT NULL,
  `recent` tinyint(4) DEFAULT NULL,
  `flagged` tinyint(4) DEFAULT NULL,
  `answered` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `seen` tinyint(4) DEFAULT NULL,
  `draft` tinyint(4) DEFAULT NULL,
  KEY `idx_ie_id` (`ie_id`),
  KEY `idx_mail_date` (`ie_id`,`mbox`,`senddate`),
  KEY `idx_mail_from` (`ie_id`,`mbox`,`fromaddr`),
  KEY `idx_mail_subj` (`subject`),
  KEY `idx_mail_to` (`toaddr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_cache`
--

LOCK TABLES `email_cache` WRITE;
/*!40000 ALTER TABLE `email_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_marketing`
--

DROP TABLE IF EXISTS `email_marketing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_marketing` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `from_addr` varchar(100) DEFAULT NULL,
  `reply_to_name` varchar(100) DEFAULT NULL,
  `reply_to_addr` varchar(100) DEFAULT NULL,
  `inbound_email_id` varchar(36) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `template_id` char(36) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `all_prospect_lists` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emmkt_name` (`name`),
  KEY `idx_emmkit_del` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_marketing`
--

LOCK TABLES `email_marketing` WRITE;
/*!40000 ALTER TABLE `email_marketing` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_marketing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_marketing_prospect_lists`
--

DROP TABLE IF EXISTS `email_marketing_prospect_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_marketing_prospect_lists` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `email_marketing_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email_mp_prospects` (`email_marketing_id`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_marketing_prospect_lists`
--

LOCK TABLES `email_marketing_prospect_lists` WRITE;
/*!40000 ALTER TABLE `email_marketing_prospect_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_marketing_prospect_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_templates` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `published` varchar(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `body_html` text,
  `deleted` tinyint(1) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `text_only` tinyint(1) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_template_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_templates`
--

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;
INSERT INTO `email_templates` VALUES ('87307d4f-0308-d69b-d0df-54490091073b','2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','off','System-generated password email','This template is used when the System Administrator sends a new password to a user.','New account information','\nHere is your account username and temporary password:\nUsername : $contact_user_user_name\nPassword : $contact_user_user_hash\n\n$config_site_url\n\nAfter you log in using the above password, you may be required to reset the password to one of your own choice.','<div><table width=\"550\"><tbody><tr><td><p>Here is your account username and temporary password:</p><p>Username : $contact_user_user_name </p><p>Password : $contact_user_user_hash </p><br /><p>$config_site_url</p><br /><p>After you log in using the above password, you may be required to reset the password to one of your own choice.</p>   </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,NULL),('95f4ce32-58d4-1fc6-d078-5449002a8a5a','2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','off','Forgot Password email','This template is used to send a user a link to click to reset the user\'s account password.','Reset your account password','\nYou recently requested on $contact_user_pwd_last_changed to be able to reset your account password.\n\nClick on the link below to reset your password:\n\n$contact_user_link_guid','<div><table width=\"550\"><tbody><tr><td><p>You recently requested on $contact_user_pwd_last_changed to be able to reset your account password. </p><p>Click on the link below to reset your password:</p><p> $contact_user_link_guid </p>  </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,NULL);
/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailman`
--

DROP TABLE IF EXISTS `emailman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailman` (
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  `list_id` char(36) DEFAULT NULL,
  `send_date_time` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `in_queue` tinyint(1) DEFAULT '0',
  `in_queue_date` datetime DEFAULT NULL,
  `send_attempts` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `related_id` char(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_eman_list` (`list_id`,`user_id`,`deleted`),
  KEY `idx_eman_campaign_id` (`campaign_id`),
  KEY `idx_eman_relid_reltype_id` (`related_id`,`related_type`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailman`
--

LOCK TABLES `emailman` WRITE;
/*!40000 ALTER TABLE `emailman` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `flagged` tinyint(1) DEFAULT NULL,
  `reply_to_status` tinyint(1) DEFAULT NULL,
  `intent` varchar(100) DEFAULT 'pick',
  `mailbox_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_name` (`name`),
  KEY `idx_message_id` (`message_id`),
  KEY `idx_email_parent_id` (`parent_id`),
  KEY `idx_email_assigned` (`assigned_user_id`,`type`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_beans`
--

DROP TABLE IF EXISTS `emails_beans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_beans` (
  `id` char(36) NOT NULL,
  `email_id` char(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `campaign_data` text,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emails_beans_bean_id` (`bean_id`),
  KEY `idx_emails_beans_email_bean` (`email_id`,`bean_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_beans`
--

LOCK TABLES `emails_beans` WRITE;
/*!40000 ALTER TABLE `emails_beans` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_beans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_email_addr_rel`
--

DROP TABLE IF EXISTS `emails_email_addr_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_email_addr_rel` (
  `id` char(36) NOT NULL,
  `email_id` char(36) NOT NULL,
  `address_type` varchar(4) DEFAULT NULL,
  `email_address_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_eearl_email_id` (`email_id`,`address_type`),
  KEY `idx_eearl_address_id` (`email_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_email_addr_rel`
--

LOCK TABLES `emails_email_addr_rel` WRITE;
/*!40000 ALTER TABLE `emails_email_addr_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_email_addr_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_text`
--

DROP TABLE IF EXISTS `emails_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_text` (
  `email_id` char(36) NOT NULL,
  `from_addr` varchar(255) DEFAULT NULL,
  `reply_to_addr` varchar(255) DEFAULT NULL,
  `to_addrs` text,
  `cc_addrs` text,
  `bcc_addrs` text,
  `description` longtext,
  `description_html` longtext,
  `raw_source` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`email_id`),
  KEY `emails_textfromaddr` (`from_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_text`
--

LOCK TABLES `emails_text` WRITE;
/*!40000 ALTER TABLE `emails_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields_meta_data`
--

DROP TABLE IF EXISTS `fields_meta_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields_meta_data` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vname` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `help` varchar(255) DEFAULT NULL,
  `custom_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `len` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `default_value` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `audited` tinyint(1) DEFAULT '0',
  `massupdate` tinyint(1) DEFAULT '0',
  `duplicate_merge` smallint(6) DEFAULT '0',
  `reportable` tinyint(1) DEFAULT '1',
  `importable` varchar(255) DEFAULT NULL,
  `ext1` varchar(255) DEFAULT NULL,
  `ext2` varchar(255) DEFAULT NULL,
  `ext3` varchar(255) DEFAULT NULL,
  `ext4` text,
  PRIMARY KEY (`id`),
  KEY `idx_meta_id_del` (`id`,`deleted`),
  KEY `idx_meta_cm_del` (`custom_module`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields_meta_data`
--

LOCK TABLES `fields_meta_data` WRITE;
/*!40000 ALTER TABLE `fields_meta_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `fields_meta_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders`
--

DROP TABLE IF EXISTS `folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders` (
  `id` char(36) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `folder_type` varchar(25) DEFAULT NULL,
  `parent_folder` char(36) DEFAULT NULL,
  `has_child` tinyint(1) DEFAULT '0',
  `is_group` tinyint(1) DEFAULT '0',
  `is_dynamic` tinyint(1) DEFAULT '0',
  `dynamic_query` text,
  `assign_to_id` char(36) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `modified_by` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_folder` (`parent_folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders`
--

LOCK TABLES `folders` WRITE;
/*!40000 ALTER TABLE `folders` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_rel`
--

DROP TABLE IF EXISTS `folders_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_rel` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `polymorphic_module` varchar(25) DEFAULT NULL,
  `polymorphic_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_poly_module_poly_id` (`polymorphic_module`,`polymorphic_id`),
  KEY `idx_fr_id_deleted_poly` (`folder_id`,`deleted`,`polymorphic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_rel`
--

LOCK TABLES `folders_rel` WRITE;
/*!40000 ALTER TABLE `folders_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_subscriptions`
--

DROP TABLE IF EXISTS `folders_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_subscriptions` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `assigned_user_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder_id_assigned_user_id` (`folder_id`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_subscriptions`
--

LOCK TABLES `folders_subscriptions` WRITE;
/*!40000 ALTER TABLE `folders_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_maps`
--

DROP TABLE IF EXISTS `import_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `source` varchar(36) DEFAULT NULL,
  `enclosure` varchar(1) DEFAULT ' ',
  `delimiter` varchar(1) DEFAULT ',',
  `module` varchar(36) DEFAULT NULL,
  `content` text,
  `default_values` text,
  `has_header` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_published` varchar(3) DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `idx_owner_module_name` (`assigned_user_id`,`module`,`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_maps`
--

LOCK TABLES `import_maps` WRITE;
/*!40000 ALTER TABLE `import_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email`
--

DROP TABLE IF EXISTS `inbound_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Active',
  `server_url` varchar(100) DEFAULT NULL,
  `email_user` varchar(100) DEFAULT NULL,
  `email_password` varchar(100) DEFAULT NULL,
  `port` int(5) DEFAULT NULL,
  `service` varchar(50) DEFAULT NULL,
  `mailbox` text,
  `delete_seen` tinyint(1) DEFAULT '0',
  `mailbox_type` varchar(10) DEFAULT NULL,
  `template_id` char(36) DEFAULT NULL,
  `stored_options` text,
  `group_id` char(36) DEFAULT NULL,
  `is_personal` tinyint(1) DEFAULT '0',
  `groupfolder_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email`
--

LOCK TABLES `inbound_email` WRITE;
/*!40000 ALTER TABLE `inbound_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email_autoreply`
--

DROP TABLE IF EXISTS `inbound_email_autoreply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email_autoreply` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `autoreplied_to` varchar(100) DEFAULT NULL,
  `ie_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ie_autoreplied_to` (`autoreplied_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email_autoreply`
--

LOCK TABLES `inbound_email_autoreply` WRITE;
/*!40000 ALTER TABLE `inbound_email_autoreply` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email_autoreply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email_cache_ts`
--

DROP TABLE IF EXISTS `inbound_email_cache_ts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email_cache_ts` (
  `id` varchar(255) NOT NULL,
  `ie_timestamp` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email_cache_ts`
--

LOCK TABLES `inbound_email_cache_ts` WRITE;
/*!40000 ALTER TABLE `inbound_email_cache_ts` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email_cache_ts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_queue`
--

DROP TABLE IF EXISTS `job_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_queue` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `scheduler_id` char(36) DEFAULT NULL,
  `execute_time` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `resolution` varchar(20) DEFAULT NULL,
  `message` text,
  `target` varchar(255) DEFAULT NULL,
  `data` text,
  `requeue` tinyint(1) DEFAULT '0',
  `retry_count` tinyint(4) DEFAULT NULL,
  `failure_count` tinyint(4) DEFAULT NULL,
  `job_delay` int(11) DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_status_scheduler` (`status`,`scheduler_id`),
  KEY `idx_status_time` (`status`,`execute_time`,`date_entered`),
  KEY `idx_status_entered` (`status`,`date_entered`),
  KEY `idx_status_modified` (`status`,`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_queue`
--

LOCK TABLES `job_queue` WRITE;
/*!40000 ALTER TABLE `job_queue` DISABLE KEYS */;
INSERT INTO `job_queue` VALUES ('1','104f34cb-a4fa-13b2-0fe3-544ec396c6a8','Check Inbound Mailboxes',0,'2014-10-27 22:12:01','2014-10-27 22:12:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:12:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2673',NULL),('1','10ebcddc-fd81-b606-249f-544ec2e78685','Check Inbound Mailboxes',0,'2014-10-27 22:11:01','2014-10-27 22:11:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:11:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2643',NULL),('1','134d7d3b-e7d6-2a30-3ed5-544914ea7540','Run Email Reminder Notifications',0,'2014-10-23 14:45:01','2014-10-23 14:45:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:45:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15862',NULL),('1','17f7c654-aadd-0efa-ad5c-544904db1880','Check Inbound Mailboxes',0,'2014-10-23 13:37:01','2014-10-23 13:37:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:37:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14807',NULL),('1','182a69dd-0b8b-b475-fc17-54490f6819ba','Check Inbound Mailboxes',0,'2014-10-23 14:25:02','2014-10-23 14:25:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:25:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15547',NULL),('1','18be9ba4-5251-c7e7-6009-54490e2436a8','Run Email Reminder Notifications',0,'2014-10-23 14:21:01','2014-10-23 14:21:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:21:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15489',NULL),('1','1ba90b6a-832f-2571-6d1c-54490667edca','Run Email Reminder Notifications',0,'2014-10-23 13:44:01','2014-10-23 13:44:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:44:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14949',NULL),('1','1dae8f2b-437c-f55e-60fc-5449191e39d1','Run Email Reminder Notifications',0,'2014-10-23 15:06:01','2014-10-23 15:06:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:06:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16167',NULL),('1','1de348d3-7195-f743-e153-5449159cd8c2','Check Inbound Mailboxes',0,'2014-10-23 14:51:02','2014-10-23 14:51:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:51:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15955',NULL),('1','1e770d8e-623a-5d29-98ae-544ec4a07106','Run Email Reminder Notifications',0,'2014-10-27 22:17:02','2014-10-27 22:17:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:17:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2853',NULL),('1','1ebf991f-45af-e2a6-e965-54490f514470','Check Inbound Mailboxes',0,'2014-10-23 14:22:02','2014-10-23 14:22:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:22:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15505',NULL),('1','1f080875-61ac-4b62-61d2-54490b517ea5','Check Inbound Mailboxes',0,'2014-10-23 14:07:02','2014-10-23 14:07:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:07:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15278',NULL),('1','20680b32-175a-b433-8818-54490cfdbcbb','Run Email Reminder Notifications',0,'2014-10-23 14:10:01','2014-10-23 14:10:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:10:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15332',NULL),('1','220f5209-db0c-9443-561c-5449170366c4','Run Email Reminder Notifications',0,'2014-10-23 14:59:01','2014-10-23 14:59:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:59:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16069',NULL),('1','24da0216-ccc5-a61e-d650-544912e35e27','Check Inbound Mailboxes',0,'2014-10-23 14:35:02','2014-10-23 14:35:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:35:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15710',NULL),('1','261de25d-af50-d7a6-6f3c-544907dff26c','Check Inbound Mailboxes',0,'2014-10-23 13:50:02','2014-10-23 13:50:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:50:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15033',NULL),('1','263bdada-198a-1a6a-7c79-544913189359','Check Inbound Mailboxes',0,'2014-10-23 14:40:02','2014-10-23 14:40:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:40:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15792',NULL),('1','26d89c32-7ba6-0835-cbca-544908f3d1af','Check Inbound Mailboxes',0,'2014-10-23 13:54:02','2014-10-23 13:54:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:54:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15091',NULL),('1','2b156911-78c4-86d2-8211-544904399078','Run Email Reminder Notifications',0,'2014-10-23 13:37:01','2014-10-23 13:37:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:37:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14807',NULL),('1','2cad22e2-4eeb-4d86-1edc-54490de10481','Check Inbound Mailboxes',0,'2014-10-23 14:14:02','2014-10-23 14:14:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:14:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15388',NULL),('1','2d08f19f-e818-1d9c-fb59-544ec26b4e4a','Run Email Reminder Notifications',0,'2014-10-27 22:11:01','2014-10-27 22:11:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:11:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2643',NULL),('1','2d0e4da1-46ff-85d3-718d-5449106bec81','Check Inbound Mailboxes',0,'2014-10-23 14:29:02','2014-10-23 14:29:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:29:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15603',NULL),('1','2df6320e-c281-4901-39b1-544915b45851','Run Email Reminder Notifications',0,'2014-10-23 14:51:02','2014-10-23 14:51:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:51:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15955',NULL),('1','2e37db17-24b1-3d8f-3dd3-544911ea4d6b','Check Inbound Mailboxes',0,'2014-10-23 14:33:02','2014-10-23 14:33:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:33:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15673',NULL),('1','2e6d4a46-cad7-cd63-1924-544ec428f5b6','Check Inbound Mailboxes',0,'2014-10-27 22:19:02','2014-10-27 22:19:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:19:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2947',NULL),('1','2ec4d292-e8dd-c1d6-422f-54490ba27926','Run Email Reminder Notifications',0,'2014-10-23 14:07:02','2014-10-23 14:07:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:07:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15278',NULL),('1','2ecb2284-226e-e010-8cea-54490f0b6b7d','Run Email Reminder Notifications',0,'2014-10-23 14:25:02','2014-10-23 14:25:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:25:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15547',NULL),('1','2f42790a-190c-d99e-1e43-5449157512d3','Check Inbound Mailboxes',0,'2014-10-23 14:48:02','2014-10-23 14:48:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:48:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15905',NULL),('1','2f5c50cd-0885-4051-1de6-54490f01d07a','Run Email Reminder Notifications',0,'2014-10-23 14:22:02','2014-10-23 14:22:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:22:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15505',NULL),('1','309243e1-32b0-b23b-17c0-544911ced9e9','Check Inbound Mailboxes',0,'2014-10-23 14:32:02','2014-10-23 14:32:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:32:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15648',NULL),('1','33bacb7d-e68a-53e0-f246-544ec347c5d9','Run Email Reminder Notifications',0,'2014-10-27 22:12:01','2014-10-27 22:12:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:12:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2673',NULL),('1','36151488-8dd0-942b-a762-54490fa62ed2','Check Inbound Mailboxes',0,'2014-10-23 14:26:02','2014-10-23 14:26:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:26:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15561',NULL),('1','36329387-d81b-d8e7-e507-54490ce2f99e','Check Inbound Mailboxes',0,'2014-10-23 14:11:02','2014-10-23 14:11:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:11:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15346',NULL),('1','366c9726-87c5-9cdc-a2ca-544906938b1e','Check Inbound Mailboxes',0,'2014-10-23 13:45:02','2014-10-23 13:45:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:45:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14963',NULL),('1','372dd2b0-66f1-c622-bedb-544913c980ae','Run Email Reminder Notifications',0,'2014-10-23 14:40:02','2014-10-23 14:40:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:40:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15792',NULL),('1','3861a258-2756-908f-8cc7-54490720753e','Check Inbound Mailboxes',0,'2014-10-23 13:48:02','2014-10-23 13:48:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:48:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15005',NULL),('1','387fec12-eba0-e424-cd59-54491259e267','Run Email Reminder Notifications',0,'2014-10-23 14:35:02','2014-10-23 14:35:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:35:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15710',NULL),('1','39c57e21-7461-9f5c-c39a-544ec3c4c628','Check Inbound Mailboxes',0,'2014-10-27 22:15:02','2014-10-27 22:15:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:15:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2790',NULL),('1','3a241ed9-b0bc-0200-a51c-54490da325e2','Run Email Reminder Notifications',0,'2014-10-23 14:14:02','2014-10-23 14:14:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:14:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15388',NULL),('1','3cbc1c32-2680-07f9-c6b0-544908d4ad01','Check Inbound Mailboxes',0,'2014-10-23 13:55:02','2014-10-23 13:55:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:55:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15105',NULL),('1','3d414370-bb94-4f2f-b7af-5449193c5ff5','Check Inbound Mailboxes',0,'2014-10-23 15:07:02','2014-10-23 15:07:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:07:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16181',NULL),('1','3f795ea2-ab87-2cde-eac4-5449176113a2','Check Inbound Mailboxes',0,'2014-10-23 15:00:02','2014-10-23 15:00:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:00:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16083',NULL),('1','3f9489bc-054f-5ab7-991b-544910e8e7fc','Check Inbound Mailboxes',0,'2014-10-23 14:27:02','2014-10-23 14:27:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:27:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15575',NULL),('1','406ea494-9405-9c4c-b376-544911c61b07','Run Email Reminder Notifications',0,'2014-10-23 14:33:02','2014-10-23 14:33:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:33:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15673',NULL),('1','40d5c3ce-5253-adda-05f3-544907a2218e','Run Email Reminder Notifications',0,'2014-10-23 13:50:02','2014-10-23 13:50:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:50:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15033',NULL),('1','41cfbf13-0105-2dac-7954-5449108606c6','Run Email Reminder Notifications',0,'2014-10-23 14:29:02','2014-10-23 14:29:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:29:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15603',NULL),('1','44603d46-d6f0-c85b-f568-54490817d8ca','Run Email Reminder Notifications',0,'2014-10-23 13:54:02','2014-10-23 13:54:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:54:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15091',NULL),('1','46b71ac6-b7a4-a2ea-2960-544ec45d847c','Run Email Reminder Notifications',0,'2014-10-27 22:19:02','2014-10-27 22:19:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:19:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2947',NULL),('1','482bbb6a-7a69-6e2f-ab3b-54490f9e7d3c','Run Email Reminder Notifications',0,'2014-10-23 14:26:02','2014-10-23 14:26:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:26:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15561',NULL),('1','497e9129-274b-f06d-e56b-54491647882d','Check Inbound Mailboxes',0,'2014-10-23 14:52:02','2014-10-23 14:52:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:52:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15971',NULL),('1','4a27d7ab-5b4a-94f7-6d3b-544907d9c8aa','Run Email Reminder Notifications',0,'2014-10-23 13:48:02','2014-10-23 13:48:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:48:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15005',NULL),('1','4b7107b9-c171-ba1d-e88a-544914223a54','Check Inbound Mailboxes',0,'2014-10-23 14:45:01','2014-10-23 14:45:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:45:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15862',NULL),('1','4c3a3346-f38c-52f4-b806-544906dbc05d','Run Email Reminder Notifications',0,'2014-10-23 13:45:02','2014-10-23 13:45:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:45:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14963',NULL),('1','4cc82d15-7621-0f58-d2c4-54490f5b9c28','Check Inbound Mailboxes',0,'2014-10-23 14:23:02','2014-10-23 14:23:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:23:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15519',NULL),('1','4eecb8ba-f2c1-9e85-95fb-54490a4f94b0','Check Inbound Mailboxes',0,'2014-10-23 14:04:02','2014-10-23 14:04:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:04:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15231',NULL),('1','50db3c40-47dc-0228-8730-544909cff9fc','Check Inbound Mailboxes',0,'2014-10-23 13:58:01','2014-10-23 13:58:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:58:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15147',NULL),('1','51c66962-4088-8635-2070-544911e4499f','Run Email Reminder Notifications',0,'2014-10-23 14:32:02','2014-10-23 14:32:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:32:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15648',NULL),('1','51cb95f6-df29-16c3-d3ad-544910566080','Run Email Reminder Notifications',0,'2014-10-23 14:27:02','2014-10-23 14:27:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:27:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15575',NULL),('1','51f28cf8-3329-7f90-9b55-54490c78db0e','Run Email Reminder Notifications',0,'2014-10-23 14:11:02','2014-10-23 14:11:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:11:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15346',NULL),('1','522310ac-65ea-5354-4081-54491779d6f8','Run Email Reminder Notifications',0,'2014-10-23 15:00:02','2014-10-23 15:00:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:00:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16083',NULL),('1','5237c403-67cd-00f5-3e94-54491057634f','Check Inbound Mailboxes',0,'2014-10-23 14:28:01','2014-10-23 14:28:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:28:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15589',NULL),('1','569f1722-44e4-7e23-6c01-5449120389f1','Check Inbound Mailboxes',0,'2014-10-23 14:36:01','2014-10-23 14:36:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:36:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15725',NULL),('1','5861329e-aacc-f346-8f15-544ec42b841d','Check Inbound Mailboxes',0,'2014-10-27 22:18:02','2014-10-27 22:18:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:18:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2924',NULL),('1','5c54056d-f8f6-6a34-2c76-544918f9d93c','Check Inbound Mailboxes',0,'2014-10-23 15:01:01','2014-10-23 15:01:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:01:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16097',NULL),('1','5d7d4014-3aab-abfc-d356-544907ff8264','Check Inbound Mailboxes',0,'2014-10-23 13:51:01','2014-10-23 13:51:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:51:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15048',NULL),('1','5f7083e2-3baf-c4bd-4f22-54490a777a4c','Check Inbound Mailboxes',0,'2014-10-23 14:02:01','2014-10-23 14:02:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:02:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15203',NULL),('1','612392d9-0ac3-7d78-29af-54490fde7ee4','Run Email Reminder Notifications',0,'2014-10-23 14:23:02','2014-10-23 14:23:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:23:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15519',NULL),('1','6452ff50-533e-b116-2eed-544918cba577','Check Inbound Mailboxes',0,'2014-10-23 15:02:01','2014-10-23 15:02:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:02:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16111',NULL),('1','6568b2f4-c725-7484-a2aa-544919011d71','Run Email Reminder Notifications',0,'2014-10-23 15:08:01','2014-10-23 15:08:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:08:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16195',NULL),('1','65a49d6f-007e-ebae-6db0-544ec3267a40','Run Email Reminder Notifications',0,'2014-10-27 22:15:02','2014-10-27 22:15:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:15:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2790',NULL),('1','664dec4b-46e6-706a-657b-5449101287e1','Run Email Reminder Notifications',0,'2014-10-23 14:28:01','2014-10-23 14:28:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:28:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15589',NULL),('1','67850dfa-a973-06ba-0f11-54490b98899c','Run Email Reminder Notifications',0,'2014-10-23 14:06:01','2014-10-23 14:06:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:06:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15259',NULL),('1','68a40e75-fe62-8dfb-126d-5449120adc45','Run Email Reminder Notifications',0,'2014-10-23 14:36:01','2014-10-23 14:36:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:36:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15725',NULL),('1','6938eae8-ce9a-381e-3c51-54491699a3ed','Check Inbound Mailboxes',0,'2014-10-23 14:53:01','2014-10-23 14:53:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:53:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15985',NULL),('1','6ae277d8-659d-f2ae-5b91-5449050db3c3','Check Inbound Mailboxes',0,'2014-10-23 13:40:01','2014-10-23 13:40:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:40:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14898',NULL),('1','6af23eaa-e76a-a72a-5f80-54490a7bccbd','Run Email Reminder Notifications',0,'2014-10-23 14:04:02','2014-10-23 14:04:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:04:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15231',NULL),('1','6b2791a1-7b34-1a1a-afb6-54491843804e','Check Inbound Mailboxes',0,'2014-10-23 15:04:01','2014-10-23 15:04:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:04:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16139',NULL),('1','6d163804-fe18-10ec-a047-544918a86f63','Run Email Reminder Notifications',0,'2014-10-23 15:01:01','2014-10-23 15:01:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:01:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16097',NULL),('1','6e183c56-c2eb-92b6-5b5d-5449062b7278','Check Inbound Mailboxes',0,'2014-10-23 13:46:01','2014-10-23 13:46:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:46:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14977',NULL),('1','6f3760ce-69e4-7ea3-4418-544919f5d132','Run Email Reminder Notifications',0,'2014-10-23 15:07:02','2014-10-23 15:07:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:07:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16181',NULL),('1','6fd89df3-0c8a-a715-5b7f-54491641fc27','Run Email Reminder Notifications',0,'2014-10-23 14:52:02','2014-10-23 14:52:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:52:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15971',NULL),('1','715986e0-4b15-59f2-ea27-5449086492b4','Run Email Reminder Notifications',0,'2014-10-23 13:55:02','2014-10-23 13:55:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:55:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15105',NULL),('1','71a33624-f1cd-da8a-5951-54490ae44dca','Run Email Reminder Notifications',0,'2014-10-23 14:02:01','2014-10-23 14:02:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:02:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15203',NULL),('1','72c65b12-7623-ca71-a29a-544907f562fd','Run Email Reminder Notifications',0,'2014-10-23 13:51:01','2014-10-23 13:51:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:51:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15048',NULL),('1','73427760-a140-aec3-0ae4-544909ae3f42','Check Inbound Mailboxes',0,'2014-10-23 13:59:01','2014-10-23 13:59:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:59:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15161',NULL),('1','73572ca4-bf1e-dbc4-eb93-544905cd12c5','Check Inbound Mailboxes',0,'2014-10-23 13:41:01','2014-10-23 13:41:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:41:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14914',NULL),('1','74958608-5c34-0480-8ae5-54491051e5b9','Check Inbound Mailboxes',0,'2014-10-23 14:30:01','2014-10-23 14:30:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:30:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15617',NULL),('1','75f56a00-d404-8399-f24f-544907a933d0','Check Inbound Mailboxes',0,'2014-10-23 13:49:01','2014-10-23 13:49:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:49:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15019',NULL),('1','78afa59f-ae3d-0d7d-3bfa-5449184b8d40','Run Email Reminder Notifications',0,'2014-10-23 15:04:01','2014-10-23 15:04:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:04:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16139',NULL),('1','7b61cad1-aa8a-75c9-8e73-54490b38b471','Check Inbound Mailboxes',0,'2014-10-23 14:08:01','2014-10-23 14:08:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:08:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15292',NULL),('1','8052e24d-fec8-eb55-4749-544916047efc','Check Inbound Mailboxes',0,'2014-10-23 14:54:01','2014-10-23 14:54:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:54:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15999',NULL),('1','80775261-ade9-a2d2-81dc-54491a6499bd','Check Inbound Mailboxes',0,'2014-10-23 15:09:01','2014-10-23 15:09:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:09:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16213',NULL),('1','810b637e-ed11-c8db-91b5-54491859a149','Run Email Reminder Notifications',0,'2014-10-23 15:02:01','2014-10-23 15:02:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:02:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16111',NULL),('1','82350861-708e-f5fe-dc67-54490ec2e33c','Check Inbound Mailboxes',0,'2014-10-23 14:21:01','2014-10-23 14:21:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:21:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15489',NULL),('1','829969ef-2a7b-7df8-2f45-54490c483c16','Check Inbound Mailboxes',0,'2014-10-23 14:12:01','2014-10-23 14:12:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:12:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15360',NULL),('1','8339b8c4-8934-9633-7d27-544915a14396','Run Email Reminder Notifications',0,'2014-10-23 14:48:02','2014-10-23 14:48:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:48:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15905',NULL),('1','8398b19e-b609-7f66-9e47-5449089d2643','Check Inbound Mailboxes',0,'2014-10-23 13:52:01','2014-10-23 13:52:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:52:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15063',NULL),('1','84ba9533-46b0-ee10-bb7f-544912cd3eba','Check Inbound Mailboxes',0,'2014-10-23 14:37:01','2014-10-23 14:37:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:37:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15738',NULL),('1','85335687-68bc-67f0-75c7-5449181b14b2','Check Inbound Mailboxes',0,'2014-10-23 15:03:01','2014-10-23 15:03:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:03:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16125',NULL),('1','881c079c-cf60-5cd8-9aa0-5449149faf42','Run Email Reminder Notifications',0,'2014-10-23 14:46:01','2014-10-23 14:46:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:46:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15876',NULL),('1','8821cea2-39c2-76e0-a92c-544ec3bda0ca','Check Inbound Mailboxes',0,'2014-10-27 22:13:01','2014-10-27 22:13:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:13:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2711',NULL),('1','8985bd17-3a3e-c361-2ce3-544916313e49','Run Email Reminder Notifications',0,'2014-10-23 14:53:01','2014-10-23 14:53:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:53:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15985',NULL),('1','8a04d81b-0a72-a0ef-6f33-544910c48daa','Run Email Reminder Notifications',0,'2014-10-23 14:30:01','2014-10-23 14:30:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:30:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15617',NULL),('1','8a07cc12-c95a-8b65-60c2-5449117cc526','Check Inbound Mailboxes',0,'2014-10-23 14:34:01','2014-10-23 14:34:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:34:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15692',NULL),('1','8a11927c-04e1-7b1d-d907-5449052cd50f','Run Email Reminder Notifications',0,'2014-10-23 13:41:01','2014-10-23 13:41:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:41:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14914',NULL),('1','8a50aa2a-253b-20ba-e862-544ec390ece7','Check Inbound Mailboxes',0,'2014-10-27 22:14:01','2014-10-27 22:14:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:14:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2748',NULL),('1','8a6cdb00-8618-e4bb-7d08-54490eae43ef','Check Inbound Mailboxes',0,'2014-10-23 14:19:01','2014-10-23 14:19:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:19:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15461',NULL),('1','8bfbe7ea-2a25-0009-901f-54490b5da206','Check Inbound Mailboxes',0,'2014-10-23 14:09:01','2014-10-23 14:09:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:09:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15308',NULL),('1','8c9d1a46-caf0-1273-c313-5449077f2b05','Run Email Reminder Notifications',0,'2014-10-23 13:49:01','2014-10-23 13:49:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:49:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15019',NULL),('1','8d5e0e01-91a7-095c-f5cc-54490a5a6841','Check Inbound Mailboxes',0,'2014-10-23 14:03:01','2014-10-23 14:03:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:03:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15218',NULL),('1','8df977ba-a6aa-8251-4c2c-54490d31f541','Check Inbound Mailboxes',0,'2014-10-23 14:16:01','2014-10-23 14:16:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:16:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15416',NULL),('1','8e488e60-9e72-3f65-3257-544906b8924c','Run Email Reminder Notifications',0,'2014-10-23 13:46:01','2014-10-23 13:46:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:46:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14977',NULL),('1','9003096d-5b6b-8d7c-7c8c-544905db328b','Run Email Reminder Notifications',0,'2014-10-23 13:43:01','2014-10-23 13:43:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:43:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14935',NULL),('1','90041d27-7e82-6e0b-111f-54490ec2e0bc','Run Email Reminder Notifications',0,'2014-10-23 14:18:01','2014-10-23 14:18:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:18:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15447',NULL),('1','90fb1054-7aca-41cc-0d27-5449162d4109','Run Email Reminder Notifications',0,'2014-10-23 14:54:01','2014-10-23 14:54:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:54:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15999',NULL),('1','91707838-2595-e412-4206-544ec4765912','Run Email Reminder Notifications',0,'2014-10-27 22:18:02','2014-10-27 22:18:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:18:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2924',NULL),('1','9399d810-4f33-2951-9237-54490567422a','Run Email Reminder Notifications',0,'2014-10-23 13:40:01','2014-10-23 13:40:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:40:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14898',NULL),('1','94004a64-b1f7-4376-d00c-54491afba6ab','Run Email Reminder Notifications',0,'2014-10-23 15:09:01','2014-10-23 15:09:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:09:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16213',NULL),('1','95283413-f37c-78b5-5427-54490a9b2e67','Check Inbound Mailboxes',0,'2014-10-23 14:01:01','2014-10-23 14:01:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:01:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15189',NULL),('1','96493067-5861-f133-e04b-544908e3bfd7','Check Inbound Mailboxes',0,'2014-10-23 13:56:01','2014-10-23 13:56:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:56:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15119',NULL),('1','96658bbe-7d0b-167f-1340-54490c0a33a6','Run Email Reminder Notifications',0,'2014-10-23 14:12:01','2014-10-23 14:12:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:12:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15360',NULL),('1','96f07e25-4012-6e5d-c690-544ec41b6def','Check Inbound Mailboxes',0,'2014-10-27 22:16:02','2014-10-27 22:16:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:16:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2819',NULL),('1','97fd3a45-0773-93f7-92f6-544913769b3b','Check Inbound Mailboxes',0,'2014-10-23 14:42:01','2014-10-23 14:42:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:42:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15820',NULL),('1','9805415e-a8b4-5798-53a4-54490bb601a3','Run Email Reminder Notifications',0,'2014-10-23 14:08:01','2014-10-23 14:08:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:08:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15292',NULL),('1','9838aff7-253d-5a17-d79e-54490538cc69','Check Inbound Mailboxes',0,'2014-10-23 13:42:02','2014-10-23 13:42:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:42:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14922',NULL),('1','9bf77ee8-16ac-65e9-6b61-544909b9a957','Run Email Reminder Notifications',0,'2014-10-23 13:59:01','2014-10-23 13:59:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:59:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15161',NULL),('1','9dff37f6-e70e-61cd-5441-544918107831','Run Email Reminder Notifications',0,'2014-10-23 15:03:01','2014-10-23 15:03:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:03:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16125',NULL),('1','9ec73727-c498-233f-5e12-544911a739a5','Run Email Reminder Notifications',0,'2014-10-23 14:34:01','2014-10-23 14:34:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:34:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15692',NULL),('1','9f04aa26-0c7a-5e02-bea2-54491544dbfd','Check Inbound Mailboxes',0,'2014-10-23 14:49:01','2014-10-23 14:49:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:49:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15927',NULL),('1','9fa4f209-1f2f-25ff-8024-544906898e6d','Check Inbound Mailboxes',0,'2014-10-23 13:47:01','2014-10-23 13:47:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:47:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14991',NULL),('1','a16c7959-5776-eb1c-1a42-5449135633f4','Check Inbound Mailboxes',0,'2014-10-23 14:41:01','2014-10-23 14:41:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:41:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15806',NULL),('1','a190de39-7826-58cc-f650-54490a6b900d','Run Email Reminder Notifications',0,'2014-10-23 14:03:01','2014-10-23 14:03:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:03:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15218',NULL),('1','a2360b99-952d-8b40-62b0-544ec39f4905','Run Email Reminder Notifications',0,'2014-10-27 22:14:01','2014-10-27 22:14:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:14:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2748',NULL),('1','a38d348a-ad79-5235-664f-54490dff60cd','Check Inbound Mailboxes',0,'2014-10-23 14:15:01','2014-10-23 14:15:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:15:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15403',NULL),('1','a3b01594-d1f9-37f2-6aca-544912bf3811','Check Inbound Mailboxes',0,'2014-10-23 14:38:01','2014-10-23 14:38:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:38:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15752',NULL),('1','a9bbd15e-eafd-c17a-3c6d-544ec45b4905','Run Email Reminder Notifications',0,'2014-10-27 22:16:02','2014-10-27 22:16:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:16:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2819',NULL),('1','aa67d890-fd86-8459-f570-54490bbe6738','Run Email Reminder Notifications',0,'2014-10-23 14:09:01','2014-10-23 14:09:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:09:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15308',NULL),('1','aaacd2ac-71da-60e5-f020-54490d1f9073','Run Email Reminder Notifications',0,'2014-10-23 14:16:01','2014-10-23 14:16:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:16:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15416',NULL),('1','ab9bba75-e76e-7638-34a3-544913864c44','Run Email Reminder Notifications',0,'2014-10-23 14:42:01','2014-10-23 14:42:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:42:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15820',NULL),('1','abfe5ea3-6c9e-24b2-898f-544919a14244','Check Inbound Mailboxes',0,'2014-10-23 15:05:01','2014-10-23 15:05:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:05:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16153',NULL),('1','ad08c3cf-a365-7af3-00c5-54490ee64247','Run Email Reminder Notifications',0,'2014-10-23 14:19:01','2014-10-23 14:19:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:19:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15461',NULL),('1','ad3ae661-56cf-1992-ad2d-54490db07e45','Check Inbound Mailboxes',0,'2014-10-23 14:17:01','2014-10-23 14:17:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:17:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15433',NULL),('1','ad631a76-a6d1-7ca3-d628-54490cc93633','Check Inbound Mailboxes',0,'2014-10-23 14:13:01','2014-10-23 14:13:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:13:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15374',NULL),('1','adbbabfb-5b9b-6e79-b751-54490872cbb3','Run Email Reminder Notifications',0,'2014-10-23 13:52:01','2014-10-23 13:52:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:52:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15063',NULL),('1','adceb14c-c53d-d0bf-ff55-544904458d98','Check Inbound Mailboxes',0,'2014-10-23 13:36:03','2014-10-23 13:36:03','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:36:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14785',NULL),('1','af8d57e4-5cef-29b1-4195-544912c580b8','Run Email Reminder Notifications',0,'2014-10-23 14:37:01','2014-10-23 14:37:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:37:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15738',NULL),('1','b163c65a-5e5b-93cd-ec53-5449145eb213','Check Inbound Mailboxes',0,'2014-10-23 14:44:01','2014-10-23 14:44:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:44:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15849',NULL),('1','b21a13da-0b0c-78d3-9c95-544919d88eb6','Check Inbound Mailboxes',0,'2014-10-23 15:08:01','2014-10-23 15:08:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:08:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16195',NULL),('1','b3b70431-c392-246f-a25f-544917ce592c','Run Email Reminder Notifications',0,'2014-10-23 14:57:01','2014-10-23 14:57:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:57:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16041',NULL),('1','b3c3a662-c6f7-3a68-dbf7-54490d68a99d','Run Email Reminder Notifications',0,'2014-10-23 14:15:01','2014-10-23 14:15:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:15:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15403',NULL),('1','b55382df-9828-e706-75ba-544905f60a22','Run Email Reminder Notifications',0,'2014-10-23 13:42:02','2014-10-23 13:42:02','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:42:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14922',NULL),('1','b5664398-bcb4-0a70-f697-5449139343e0','Run Email Reminder Notifications',0,'2014-10-23 14:41:01','2014-10-23 14:41:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:41:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15806',NULL),('1','b590a143-48a4-61b2-d750-544904b701e7','Check Inbound Mailboxes',0,'2014-10-23 13:38:01','2014-10-23 13:38:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:38:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14842',NULL),('1','bc609f74-bc07-f129-64b5-54490d790e98','Run Email Reminder Notifications',0,'2014-10-23 14:17:01','2014-10-23 14:17:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:17:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15433',NULL),('1','bc83e3fa-ec85-b505-a391-54491360bfed','Check Inbound Mailboxes',0,'2014-10-23 14:43:01','2014-10-23 14:43:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:43:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15834',NULL),('1','bd758003-dcce-0b79-dfe7-54490c509226','Run Email Reminder Notifications',0,'2014-10-23 14:13:01','2014-10-23 14:13:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:13:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15374',NULL),('1','bf2a1f58-8938-5382-9b19-544906400c3b','Run Email Reminder Notifications',0,'2014-10-23 13:47:01','2014-10-23 13:47:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:47:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14991',NULL),('1','bf66c279-46c7-17df-97fe-54490422fe96','Check Inbound Mailboxes',0,'2014-10-23 13:39:01','2014-10-23 13:39:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:39:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14858',NULL),('1','c1f5593a-f63e-a594-4eb4-54490f1e4078','Check Inbound Mailboxes',0,'2014-10-23 14:24:01','2014-10-23 14:24:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:24:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15534',NULL),('1','c308e803-05da-98f6-998e-5449199e43ed','Run Email Reminder Notifications',0,'2014-10-23 15:05:01','2014-10-23 15:05:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 15:05:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16153',NULL),('1','c333da32-ca90-0c82-aed5-5449045c7030','Run Nightly Process Bounced Campaign Emails',0,'2014-10-23 13:36:03','2014-10-23 13:36:03','cdc75f4a-15e1-ecc7-f483-544900af2037','2014-10-23 13:36:00','done','success',NULL,'function::pollMonitoredInboxesForBouncedCampaignEmails',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14785',NULL),('1','c60aa7f7-fd73-1ac0-64fb-54490867d19f','Run Email Reminder Notifications',0,'2014-10-23 13:56:01','2014-10-23 13:56:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:56:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15119',NULL),('1','c70db15f-c5c1-e875-cbf8-544915e044d3','Run Email Reminder Notifications',0,'2014-10-23 14:49:01','2014-10-23 14:49:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:49:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15927',NULL),('1','c9e46767-7a89-5fba-008b-544911e653b3','Check Inbound Mailboxes',0,'2014-10-23 14:31:01','2014-10-23 14:31:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:31:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15631',NULL),('1','ca7a3d06-a3f4-f26c-1443-544916332581','Check Inbound Mailboxes',0,'2014-10-23 14:55:01','2014-10-23 14:55:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:55:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16013',NULL),('1','cab69276-db60-a0d5-f07d-54490a52f412','Run Email Reminder Notifications',0,'2014-10-23 14:01:01','2014-10-23 14:01:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:01:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15189',NULL),('1','cebfec75-4476-033b-d7a2-544ec5af991c','Check Inbound Mailboxes',0,'2014-10-27 22:20:01','2014-10-27 22:20:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:20:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2966',NULL),('1','cf68c91a-1002-1eb2-159e-5449176f7c54','Check Inbound Mailboxes',0,'2014-10-23 14:56:01','2014-10-23 14:56:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:56:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16027',NULL),('1','d1e5984a-67bf-3a53-451c-54491331de1b','Run Email Reminder Notifications',0,'2014-10-23 14:43:01','2014-10-23 14:43:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:43:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15834',NULL),('1','d3aa2ced-7673-5fef-56eb-544919c06872','Check Inbound Mailboxes',0,'2014-10-23 15:06:01','2014-10-23 15:06:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 15:06:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16167',NULL),('1','d432a1d8-4e8e-21e2-2151-54490fef0fa9','Run Email Reminder Notifications',0,'2014-10-23 14:24:01','2014-10-23 14:24:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:24:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15534',NULL),('1','d5012410-9876-56e9-baaa-54490bb19dc4','Check Inbound Mailboxes',0,'2014-10-23 14:05:01','2014-10-23 14:05:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:05:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15245',NULL),('1','d6406507-ae2a-9e4b-7fd4-544ec466ca77','Check Inbound Mailboxes',0,'2014-10-27 22:17:02','2014-10-27 22:17:02','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-27 22:17:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2853',NULL),('1','d7230556-28f2-84f5-b8f0-544917c12a1f','Check Inbound Mailboxes',0,'2014-10-23 14:59:01','2014-10-23 14:59:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:59:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16069',NULL),('1','d7c54741-b069-b4f8-f693-5449059e3fd1','Check Inbound Mailboxes',0,'2014-10-23 13:43:01','2014-10-23 13:43:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:43:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14935',NULL),('1','da7de435-c659-a767-7c32-54490e01a6c0','Check Inbound Mailboxes',0,'2014-10-23 14:20:01','2014-10-23 14:20:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:20:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15475',NULL),('1','da909cc5-b3ab-6e22-f43b-54491784c781','Check Inbound Mailboxes',0,'2014-10-23 14:57:01','2014-10-23 14:57:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:57:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16041',NULL),('1','daa6c8c7-f4c9-46a2-0ac1-54490449228c','Run Email Reminder Notifications',0,'2014-10-23 13:39:01','2014-10-23 13:39:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:39:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14858',NULL),('1','db5364d3-f51c-8877-cc6f-544ec37ad87b','Run Email Reminder Notifications',0,'2014-10-27 22:13:01','2014-10-27 22:13:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:13:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2711',NULL),('1','db796774-e991-3bb8-da95-544908287532','Check Inbound Mailboxes',0,'2014-10-23 13:53:01','2014-10-23 13:53:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:53:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15077',NULL),('1','dbdeb4a5-2ff9-f077-a98d-544916a8da99','Run Email Reminder Notifications',0,'2014-10-23 14:55:01','2014-10-23 14:55:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:55:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16013',NULL),('1','dc05dac1-f2f7-9ba1-275f-544904695d78','Run Email Reminder Notifications',0,'2014-10-23 13:38:01','2014-10-23 13:38:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:38:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14842',NULL),('1','dc73c044-719f-2a25-55cb-5449049a7171','Run Nightly Mass Email Campaigns',0,'2014-10-23 13:36:03','2014-10-23 13:36:03','e0162c62-d498-5e4e-6218-544900046e1b','2014-10-23 13:36:00','done','success',NULL,'function::runMassEmailCampaign',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14785',NULL),('1','dd3e2760-f545-d60e-9ca0-544909ccaf7a','Check Inbound Mailboxes',0,'2014-10-23 13:57:01','2014-10-23 13:57:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:57:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15134',NULL),('1','dd8cde71-c044-a406-a4db-5449097455ae','Check Inbound Mailboxes',0,'2014-10-23 14:00:01','2014-10-23 14:00:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:00:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15175',NULL),('1','df20c626-b277-5fc0-620a-544911e6855d','Run Email Reminder Notifications',0,'2014-10-23 14:31:01','2014-10-23 14:31:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:31:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15631',NULL),('1','dfedf84d-e2d6-57bc-5c2d-54491324c327','Check Inbound Mailboxes',0,'2014-10-23 14:39:01','2014-10-23 14:39:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:39:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15768',NULL),('1','e0290717-03e3-e6ce-3eb5-54491560927e','Run Email Reminder Notifications',0,'2014-10-23 14:50:01','2014-10-23 14:50:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:50:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15942',NULL),('1','e2070e31-4c82-7460-3e56-544917461c6a','Run Email Reminder Notifications',0,'2014-10-23 14:58:01','2014-10-23 14:58:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:58:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16056',NULL),('1','e22ec062-b4f1-b5f4-316c-5449140a036b','Run Email Reminder Notifications',0,'2014-10-23 14:44:01','2014-10-23 14:44:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:44:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15849',NULL),('1','e2fdf07e-eff9-cdfa-0124-5449064734ad','Check Inbound Mailboxes',0,'2014-10-23 13:44:01','2014-10-23 13:44:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 13:44:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14949',NULL),('1','e545435d-dbf6-ec78-b673-54490b516a20','Run Email Reminder Notifications',0,'2014-10-23 14:05:01','2014-10-23 14:05:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:05:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15245',NULL),('1','e6035303-4975-f2f5-f604-544ec5a25ae6','Run Email Reminder Notifications',0,'2014-10-27 22:20:01','2014-10-27 22:20:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-27 22:20:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:2966',NULL),('1','e6e25293-ff57-1e25-9aee-544914a80bc0','Check Inbound Mailboxes',0,'2014-10-23 14:46:01','2014-10-23 14:46:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:46:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15876',NULL),('1','e7274a5f-0bcc-9578-fa8d-544904677506','Run Email Reminder Notifications',0,'2014-10-23 13:36:03','2014-10-23 13:36:03','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:36:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:14785',NULL),('1','e8ee5aa8-444d-46de-5573-54490b355609','Check Inbound Mailboxes',0,'2014-10-23 14:06:01','2014-10-23 14:06:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:06:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15259',NULL),('1','eb5ba261-6bc9-11f7-fbd0-54490ec658ca','Check Inbound Mailboxes',0,'2014-10-23 14:18:01','2014-10-23 14:18:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:18:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15447',NULL),('1','eb654f42-5d50-117c-bce3-544909e350c3','Run Email Reminder Notifications',0,'2014-10-23 13:58:01','2014-10-23 13:58:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:58:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15147',NULL),('1','eccc0bfd-ef50-3414-50de-54491232c48f','Run Email Reminder Notifications',0,'2014-10-23 14:38:01','2014-10-23 14:38:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:38:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15752',NULL),('1','ede7a2af-3573-c2fd-7d19-54490efc8fdc','Run Email Reminder Notifications',0,'2014-10-23 14:20:01','2014-10-23 14:20:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:20:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15475',NULL),('1','ee0e9053-92b3-d302-c294-5449151e2e3d','Check Inbound Mailboxes',0,'2014-10-23 14:50:01','2014-10-23 14:50:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:50:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15942',NULL),('1','ee65cded-4193-43d8-a5df-5449088a3c87','Run Email Reminder Notifications',0,'2014-10-23 13:53:01','2014-10-23 13:53:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:53:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15077',NULL),('1','eecb17ed-38d2-18cf-3072-544909903ba8','Run Email Reminder Notifications',0,'2014-10-23 13:57:01','2014-10-23 13:57:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 13:57:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15134',NULL),('1','efb3912f-b6e5-e93f-3b02-544909490edb','Run Email Reminder Notifications',0,'2014-10-23 14:00:01','2014-10-23 14:00:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:00:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15175',NULL),('1','efde8247-6440-4dbe-bcb1-544913fd18fe','Run Email Reminder Notifications',0,'2014-10-23 14:39:01','2014-10-23 14:39:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:39:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15768',NULL),('1','f03012b4-823e-71f2-3235-544917e25361','Run Email Reminder Notifications',0,'2014-10-23 14:56:01','2014-10-23 14:56:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:56:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16027',NULL),('1','f22606f0-8020-babe-1188-54490c2b522d','Check Inbound Mailboxes',0,'2014-10-23 14:10:01','2014-10-23 14:10:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:10:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15332',NULL),('1','f261f7c0-b229-65a5-c0f8-544914c5125b','Check Inbound Mailboxes',0,'2014-10-23 14:47:01','2014-10-23 14:47:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:47:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15890',NULL),('1','f367894c-dded-a012-8dd8-544917dd1e63','Check Inbound Mailboxes',0,'2014-10-23 14:58:01','2014-10-23 14:58:01','b5e4d5ed-dac1-ca6c-adb2-544900b854e0','2014-10-23 14:58:00','done','success',NULL,'function::pollMonitoredInboxes',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:16056',NULL),('1','faf906c5-26df-41f3-74b9-5449142b026a','Run Email Reminder Notifications',0,'2014-10-23 14:47:01','2014-10-23 14:47:01','ee51b51d-842a-7028-cc7b-5449003170ee','2014-10-23 14:47:00','done','success',NULL,'function::sendEmailReminders',NULL,0,NULL,NULL,0,'CRON1a6682f3d09851304a548eaf7bb03df2:15890',NULL);
/*!40000 ALTER TABLE `job_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads`
--

DROP TABLE IF EXISTS `leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `converted` tinyint(1) DEFAULT '0',
  `refered_by` varchar(100) DEFAULT NULL,
  `lead_source` varchar(100) DEFAULT NULL,
  `lead_source_description` text,
  `status` varchar(100) DEFAULT NULL,
  `status_description` text,
  `reports_to_id` char(36) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_description` text,
  `contact_id` char(36) DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `opportunity_name` varchar(255) DEFAULT NULL,
  `opportunity_amount` varchar(50) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `portal_name` varchar(255) DEFAULT NULL,
  `portal_app` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lead_acct_name_first` (`account_name`,`deleted`),
  KEY `idx_lead_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_lead_del_stat` (`last_name`,`status`,`deleted`,`first_name`),
  KEY `idx_lead_opp_del` (`opportunity_id`,`deleted`),
  KEY `idx_leads_acct_del` (`account_id`,`deleted`),
  KEY `idx_del_user` (`deleted`,`assigned_user_id`),
  KEY `idx_lead_assigned` (`assigned_user_id`),
  KEY `idx_lead_contact` (`contact_id`),
  KEY `idx_reports_to` (`reports_to_id`),
  KEY `idx_lead_phone_work` (`phone_work`),
  KEY `idx_leads_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads`
--

LOCK TABLES `leads` WRITE;
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_audit`
--

DROP TABLE IF EXISTS `leads_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_leads_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_audit`
--

LOCK TABLES `leads_audit` WRITE;
/*!40000 ALTER TABLE `leads_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linked_documents`
--

DROP TABLE IF EXISTS `linked_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linked_documents` (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_document` (`parent_type`,`parent_id`,`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linked_documents`
--

LOCK TABLES `linked_documents` WRITE;
/*!40000 ALTER TABLE `linked_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `linked_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings`
--

DROP TABLE IF EXISTS `meetings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `join_url` varchar(200) DEFAULT NULL,
  `host_url` varchar(400) DEFAULT NULL,
  `displayed_url` varchar(400) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `external_id` varchar(50) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `type` varchar(255) DEFAULT 'Sugar',
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT '0',
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_mtg_name` (`name`),
  KEY `idx_meet_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_meet_stat_del` (`assigned_user_id`,`status`,`deleted`),
  KEY `idx_meet_date_start` (`date_start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings`
--

LOCK TABLES `meetings` WRITE;
/*!40000 ALTER TABLE `meetings` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_contacts`
--

DROP TABLE IF EXISTS `meetings_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_contacts` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_mtg_mtg` (`meeting_id`),
  KEY `idx_con_mtg_con` (`contact_id`),
  KEY `idx_meeting_contact` (`meeting_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_contacts`
--

LOCK TABLES `meetings_contacts` WRITE;
/*!40000 ALTER TABLE `meetings_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_leads`
--

DROP TABLE IF EXISTS `meetings_leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_leads` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_meeting_meeting` (`meeting_id`),
  KEY `idx_lead_meeting_lead` (`lead_id`),
  KEY `idx_meeting_lead` (`meeting_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_leads`
--

LOCK TABLES `meetings_leads` WRITE;
/*!40000 ALTER TABLE `meetings_leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_users`
--

DROP TABLE IF EXISTS `meetings_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_users` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_mtg_mtg` (`meeting_id`),
  KEY `idx_usr_mtg_usr` (`user_id`),
  KEY `idx_meeting_users` (`meeting_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_users`
--

LOCK TABLES `meetings_users` WRITE;
/*!40000 ALTER TABLE `meetings_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `portal_flag` tinyint(1) DEFAULT NULL,
  `embed_flag` tinyint(1) DEFAULT '0',
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_note_name` (`name`),
  KEY `idx_notes_parent` (`parent_id`,`parent_type`),
  KEY `idx_note_contact` (`contact_id`),
  KEY `idx_notes_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_consumer`
--

DROP TABLE IF EXISTS `oauth_consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_consumer` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `c_key` varchar(255) DEFAULT NULL,
  `c_secret` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ckey` (`c_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_consumer`
--

LOCK TABLES `oauth_consumer` WRITE;
/*!40000 ALTER TABLE `oauth_consumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_consumer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_nonce`
--

DROP TABLE IF EXISTS `oauth_nonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_nonce` (
  `conskey` varchar(32) NOT NULL,
  `nonce` varchar(32) NOT NULL,
  `nonce_ts` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`conskey`,`nonce`),
  KEY `oauth_nonce_keyts` (`conskey`,`nonce_ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_nonce`
--

LOCK TABLES `oauth_nonce` WRITE;
/*!40000 ALTER TABLE `oauth_nonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_nonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_tokens`
--

DROP TABLE IF EXISTS `oauth_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_tokens` (
  `id` char(36) NOT NULL,
  `secret` varchar(32) DEFAULT NULL,
  `tstate` varchar(1) DEFAULT NULL,
  `consumer` char(36) NOT NULL,
  `token_ts` bigint(20) DEFAULT NULL,
  `verify` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `callback_url` varchar(255) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`,`deleted`),
  KEY `oauth_state_ts` (`tstate`,`token_ts`),
  KEY `constoken_key` (`consumer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_tokens`
--

LOCK TABLES `oauth_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities`
--

DROP TABLE IF EXISTS `opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `opportunity_type` varchar(255) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `lead_source` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_usdollar` double DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `date_closed` date DEFAULT NULL,
  `next_step` varchar(100) DEFAULT NULL,
  `sales_stage` varchar(255) DEFAULT NULL,
  `probability` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_opp_name` (`name`),
  KEY `idx_opp_assigned` (`assigned_user_id`),
  KEY `idx_opp_id_deleted` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities`
--

LOCK TABLES `opportunities` WRITE;
/*!40000 ALTER TABLE `opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities_audit`
--

DROP TABLE IF EXISTS `opportunities_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_opportunities_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_audit`
--

LOCK TABLES `opportunities_audit` WRITE;
/*!40000 ALTER TABLE `opportunities_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities_contacts`
--

DROP TABLE IF EXISTS `opportunities_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_opp_con` (`contact_id`),
  KEY `idx_con_opp_opp` (`opportunity_id`),
  KEY `idx_opportunities_contacts` (`opportunity_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_contacts`
--

LOCK TABLES `opportunities_contacts` WRITE;
/*!40000 ALTER TABLE `opportunities_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outbound_email`
--

DROP TABLE IF EXISTS `outbound_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outbound_email` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(15) DEFAULT 'user',
  `user_id` char(36) NOT NULL,
  `mail_sendtype` varchar(8) DEFAULT 'smtp',
  `mail_smtptype` varchar(20) DEFAULT 'other',
  `mail_smtpserver` varchar(100) DEFAULT NULL,
  `mail_smtpport` int(5) DEFAULT '0',
  `mail_smtpuser` varchar(100) DEFAULT NULL,
  `mail_smtppass` varchar(100) DEFAULT NULL,
  `mail_smtpauth_req` tinyint(1) DEFAULT '0',
  `mail_smtpssl` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `oe_user_id_idx` (`id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outbound_email`
--

LOCK TABLES `outbound_email` WRITE;
/*!40000 ALTER TABLE `outbound_email` DISABLE KEYS */;
INSERT INTO `outbound_email` VALUES ('ad344762-bbe0-d1e6-3e77-5449009fed10','system','system','1','SMTP','gmail','smtp.gmail.com',587,'janis@itrisinajumi.lv','LL5S3b5uSUE=',1,2);
/*!40000 ALTER TABLE `outbound_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `estimated_start_date` date DEFAULT NULL,
  `estimated_end_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task`
--

DROP TABLE IF EXISTS `project_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_task` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `project_id` char(36) NOT NULL,
  `project_task_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `description` text,
  `predecessors` text,
  `date_start` date DEFAULT NULL,
  `time_start` int(11) DEFAULT NULL,
  `time_finish` int(11) DEFAULT NULL,
  `date_finish` date DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `duration_unit` text,
  `actual_duration` int(11) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  `date_due` date DEFAULT NULL,
  `time_due` time DEFAULT NULL,
  `parent_task_id` int(11) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT NULL,
  `order_number` int(11) DEFAULT '1',
  `task_number` int(11) DEFAULT NULL,
  `estimated_effort` int(11) DEFAULT NULL,
  `actual_effort` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `utilization` int(11) DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task`
--

LOCK TABLES `project_task` WRITE;
/*!40000 ALTER TABLE `project_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task_audit`
--

DROP TABLE IF EXISTS `project_task_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_task_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_project_task_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task_audit`
--

LOCK TABLES `project_task_audit` WRITE;
/*!40000 ALTER TABLE `project_task_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_task_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_accounts`
--

DROP TABLE IF EXISTS `projects_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_accounts` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_acct_proj` (`project_id`),
  KEY `idx_proj_acct_acct` (`account_id`),
  KEY `projects_accounts_alt` (`project_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_accounts`
--

LOCK TABLES `projects_accounts` WRITE;
/*!40000 ALTER TABLE `projects_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_bugs`
--

DROP TABLE IF EXISTS `projects_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_bugs` (
  `id` varchar(36) NOT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_bug_proj` (`project_id`),
  KEY `idx_proj_bug_bug` (`bug_id`),
  KEY `projects_bugs_alt` (`project_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_bugs`
--

LOCK TABLES `projects_bugs` WRITE;
/*!40000 ALTER TABLE `projects_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_cases`
--

DROP TABLE IF EXISTS `projects_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_cases` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_case_proj` (`project_id`),
  KEY `idx_proj_case_case` (`case_id`),
  KEY `projects_cases_alt` (`project_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_cases`
--

LOCK TABLES `projects_cases` WRITE;
/*!40000 ALTER TABLE `projects_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_contacts`
--

DROP TABLE IF EXISTS `projects_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_con_proj` (`project_id`),
  KEY `idx_proj_con_con` (`contact_id`),
  KEY `projects_contacts_alt` (`project_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_contacts`
--

LOCK TABLES `projects_contacts` WRITE;
/*!40000 ALTER TABLE `projects_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_opportunities`
--

DROP TABLE IF EXISTS `projects_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_opp_proj` (`project_id`),
  KEY `idx_proj_opp_opp` (`opportunity_id`),
  KEY `projects_opportunities_alt` (`project_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_opportunities`
--

LOCK TABLES `projects_opportunities` WRITE;
/*!40000 ALTER TABLE `projects_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_products`
--

DROP TABLE IF EXISTS `projects_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_products` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_prod_project` (`project_id`),
  KEY `idx_proj_prod_product` (`product_id`),
  KEY `projects_products_alt` (`project_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_products`
--

LOCK TABLES `projects_products` WRITE;
/*!40000 ALTER TABLE `projects_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_list_campaigns`
--

DROP TABLE IF EXISTS `prospect_list_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_list_campaigns` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `campaign_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_pro_id` (`prospect_list_id`),
  KEY `idx_cam_id` (`campaign_id`),
  KEY `idx_prospect_list_campaigns` (`prospect_list_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_list_campaigns`
--

LOCK TABLES `prospect_list_campaigns` WRITE;
/*!40000 ALTER TABLE `prospect_list_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_list_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_lists`
--

DROP TABLE IF EXISTS `prospect_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_lists` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_type` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `description` text,
  `domain_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_prospect_list_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_lists`
--

LOCK TABLES `prospect_lists` WRITE;
/*!40000 ALTER TABLE `prospect_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_lists_prospects`
--

DROP TABLE IF EXISTS `prospect_lists_prospects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_lists_prospects` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_plp_pro_id` (`prospect_list_id`),
  KEY `idx_plp_rel_id` (`related_id`,`related_type`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_lists_prospects`
--

LOCK TABLES `prospect_lists_prospects` WRITE;
/*!40000 ALTER TABLE `prospect_lists_prospects` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_lists_prospects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospects`
--

DROP TABLE IF EXISTS `prospects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospects` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` date DEFAULT NULL,
  `lead_id` char(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_auto_tracker_key` (`tracker_key`),
  KEY `idx_prospects_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_prospecs_del_last` (`last_name`,`deleted`),
  KEY `idx_prospects_id_del` (`id`,`deleted`),
  KEY `idx_prospects_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospects`
--

LOCK TABLES `prospects` WRITE;
/*!40000 ALTER TABLE `prospects` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationships`
--

DROP TABLE IF EXISTS `relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationships` (
  `id` char(36) NOT NULL,
  `relationship_name` varchar(150) DEFAULT NULL,
  `lhs_module` varchar(100) DEFAULT NULL,
  `lhs_table` varchar(64) DEFAULT NULL,
  `lhs_key` varchar(64) DEFAULT NULL,
  `rhs_module` varchar(100) DEFAULT NULL,
  `rhs_table` varchar(64) DEFAULT NULL,
  `rhs_key` varchar(64) DEFAULT NULL,
  `join_table` varchar(128) DEFAULT NULL,
  `join_key_lhs` varchar(128) DEFAULT NULL,
  `join_key_rhs` varchar(128) DEFAULT NULL,
  `relationship_type` varchar(64) DEFAULT NULL,
  `relationship_role_column` varchar(64) DEFAULT NULL,
  `relationship_role_column_value` varchar(50) DEFAULT NULL,
  `reverse` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_rel_name` (`relationship_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationships`
--

LOCK TABLES `relationships` WRITE;
/*!40000 ALTER TABLE `relationships` DISABLE KEYS */;
INSERT INTO `relationships` VALUES ('108fc6bf-8eca-22ae-8d1d-5449004e7ba9','accounts_opportunities','Accounts','accounts','id','Opportunities','opportunities','id','accounts_opportunities','account_id','opportunity_id','many-to-many',NULL,NULL,0,0),('112fbc84-3e64-03ad-377d-5448ffc566e8','contact_tasks','Contacts','contacts','id','Tasks','tasks','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('11620eee-e140-e1b5-b7d3-544900d1658a','emails_project_task_rel','Emails','emails','id','ProjectTask','project_task','id','emails_beans','email_id','bean_id','many-to-many','bean_module','ProjectTask',0,0),('11cba549-b8fe-7308-ce86-5448ffe971d8','account_cases','Accounts','accounts','id','Cases','cases','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('12cad665-e067-26a9-8b2f-5448ffdb79bc','projects_notes','Project','project','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('12e3af94-d17b-47d2-906e-544900750d09','calls_contacts','Calls','calls','id','Contacts','contacts','id','calls_contacts','call_id','contact_id','many-to-many',NULL,NULL,0,0),('13314c10-2352-7da9-7322-54490050ea92','projects_cases','Project','project','id','Cases','cases','id','projects_cases','project_id','case_id','many-to-many',NULL,NULL,0,0),('13beacb7-8c10-8df3-fe44-544900b1d63d','sugarfeed_assigned_user','Users','users','id','SugarFeed','sugarfeed','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('14095a45-90a0-5875-57d4-54490067760d','calls_notes','Calls','calls','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Calls',0,0),('14e2bafd-76ca-b85a-af20-5448ff8171e4','campaign_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1579bc56-ccbc-785a-5572-5448ffd013df','project_tasks_notes','ProjectTask','project_task','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('15bf579c-368e-446b-028e-5449007f3ea6','tasks_modified_user','Users','users','id','Tasks','tasks','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1668e3b0-c45e-170c-efd9-5448ff82f486','users_email_addresses','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Users',0,0),('17bf1ff4-8b20-a8e8-18ac-54490064cc85','oauthkeys_modified_user','Users','users','id','OAuthKeys','oauthkeys','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('17f0421e-990b-4a50-c4d1-5448ffa20c4c','account_tasks','Accounts','accounts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('1821347c-cd0f-ac14-cb24-5448ffe9601a','contact_tasks_parent','Contacts','contacts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),('192a30d5-4891-acc0-e0c2-5448ff20d53c','prospect_notes','Prospects','prospects','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('1b1a2984-70bb-e72e-0033-54490047df4c','documents_created_by','Users','users','id','Documents','documents','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1b424fb6-28b1-67e3-8a02-5448ff9fe215','projects_tasks','Project','project','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('1c09984c-d5b0-f4a0-1dc7-544900c5e218','contacts_cases','Contacts','contacts','id','Cases','cases','id','contacts_cases','contact_id','case_id','many-to-many',NULL,NULL,0,0),('1d9db4b6-e111-39b7-8b8d-54490047e1fa','emails_projects_rel','Emails','emails','id','Project','project','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Project',0,0),('1de8f2e9-5223-0ea0-3847-544900768d8a','saved_search_assigned_user','Users','users','id','SavedSearch','saved_search','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1de99746-95cc-c350-9114-5448ffc47d03','bug_notes','Bugs','bugs','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('1ee025fa-ba0f-bb61-8824-54490093c37b','tasks_created_by','Users','users','id','Tasks','tasks','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1f3fe656-356d-4c58-3ba5-5448ffc356f4','account_notes','Accounts','accounts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('1f73363d-29e2-72d6-9c53-5448ffb9eb60','contact_notes_parent','Contacts','contacts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),('1fba5a8e-564b-6c16-afff-544900f97579','oauthkeys_created_by','Users','users','id','OAuthKeys','oauthkeys','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1ff519c9-b7db-7ac9-cf14-5448ff5cc7d4','prospect_meetings','Prospects','prospects','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('2206b410-0b45-1be7-9552-5448ff1354b1','project_tasks_tasks','ProjectTask','project_task','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('22640b0b-f003-13c4-e6d0-54490001ade9','accounts_bugs','Accounts','accounts','id','Bugs','bugs','id','accounts_bugs','account_id','bug_id','many-to-many',NULL,NULL,0,0),('23bb8b06-f6e7-ce3c-b0d2-544900c9c4f8','emails_prospects_rel','Emails','emails','id','Prospects','prospects','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Prospects',0,0),('2578f94e-e75c-e364-ecd7-5448ff7d11a1','projects_meetings','Project','project','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('25da4d9d-fb7f-adf3-7ebf-5449002f3db9','oauthkeys_assigned_user','Users','users','id','OAuthKeys','oauthkeys','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('25dc1f3f-2d4f-2eae-9a5b-5449008aad8a','tasks_assigned_user','Users','users','id','Tasks','tasks','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('26103d59-3d61-ba25-ec9e-5448ff944e3d','prospect_calls','Prospects','prospects','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('266bd391-eef7-f9e2-a5a9-5448ff978110','contact_campaign_log','Contacts','contacts','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Contacts',0,0),('279f0753-3221-effc-1915-544900f5599d','projects_contacts','Project','project','id','Contacts','contacts','id','projects_contacts','project_id','contact_id','many-to-many',NULL,NULL,0,0),('2849cb4a-225b-a643-fe2d-5448ffcaab01','account_meetings','Accounts','accounts','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('285a0124-b346-5568-4e02-5448ff7bf433','campaign_emailman','Campaigns','campaigns','id','EmailMan','emailman','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('28e8ef7d-78fe-5ce0-0a42-5448ff8d0f58','users_email_addresses_primary','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('29a98fb0-c36c-aee7-6f5c-5448ffc60fb8','project_tasks_meetings','ProjectTask','project_task','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('2a1f7343-022d-42be-4d2f-5448ffb0fa70','bugs_release','Releases','releases','id','Bugs','bugs','found_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2bf855df-c3d2-866f-6985-5449006118c6','tasks_notes','Tasks','tasks','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2c3286b0-7149-061a-d912-5448ff2c39e3','prospect_emails','Prospects','prospects','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('2e616961-5f5b-f2b0-ab8f-5448ff7c6c6e','account_calls','Accounts','accounts','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('30495011-e649-4102-01e5-5448ff5fc8f0','project_tasks_calls','ProjectTask','project_task','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('309b227b-9da4-6fcd-b5c5-5448ffdb8c04','bugs_fixed_in_release','Releases','releases','id','Bugs','bugs','fixed_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('31427e4b-d768-2894-e02b-5448ff43599b','projects_calls','Project','project','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('3492e288-1a0f-f110-4dde-544900bb8444','contacts_users','Contacts','contacts','id','Users','users','id','contacts_users','contact_id','user_id','many-to-many',NULL,NULL,0,0),('35bd222a-fc2b-ab04-f01f-5448ff541dac','prospect_campaign_log','Prospects','prospects','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Prospects',0,0),('36640cd2-f549-8abc-a648-5448ff62907a','prospects_email_addresses_primary','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('38a10956-27cb-b344-b6e2-5448ff504feb','accounts_email_addresses_primary','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('390d0812-c6ba-dc15-7932-544900c2db18','contacts_bugs','Contacts','contacts','id','Bugs','bugs','id','contacts_bugs','contact_id','bug_id','many-to-many',NULL,NULL,0,0),('394d05dc-b118-af87-cf4d-5449001f9b00','emails_leads_rel','Emails','emails','id','Leads','leads','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Leads',0,0),('39f410a5-99a1-59c7-2716-5448ff6d93a4','cases_modified_user','Users','users','id','Cases','cases','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('39f61a45-1b0a-d9d6-ef4a-5448ff6c04c9','project_tasks_emails','ProjectTask','project_task','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('3c407864-1933-b204-391c-5448ffdc6bf7','account_emails','Accounts','accounts','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('3ff90187-b2bd-6108-d74d-5448ff3ae288','projects_emails','Project','project','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('41de0ea1-c95b-9a6d-8074-5448ff719f3c','cases_created_by','Users','users','id','Cases','cases','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('42a41554-e09a-7918-9d46-5448ff05692f','project_tasks_assigned_user','Users','users','id','ProjectTask','project_task','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('43fd93ad-8b77-a211-6721-5448ff0d85b7','account_leads','Accounts','accounts','id','Leads','leads','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('44b06bb0-d51a-a23a-9833-544900ffadda','consumer_tokens','OAuthKeys','oauth_consumer','id','OAuthTokens','oauth_tokens','consumer',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4642030d-a177-9d57-b8b9-544900ae4bfb','projects_opportunities','Project','project','id','Opportunities','opportunities','id','projects_opportunities','project_id','opportunity_id','many-to-many',NULL,NULL,0,0),('47f878bc-83b6-a9ec-0cc7-5448ff24ffa6','cases_assigned_user','Users','users','id','Cases','cases','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('48c13546-e2f8-5dbd-1ed1-5448ff641040','project_tasks_modified_user','Users','users','id','ProjectTask','project_task','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4aab256f-0e25-886a-6c75-5449001f21ef','oauthtokens_assigned_user','Users','users','id','OAuthTokens','oauth_tokens','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4aee9f6c-4973-f929-9c0f-5448ff3aed8d','account_campaign_log','Accounts','accounts','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Accounts',0,0),('4e3fa82a-c9e4-6169-fc4d-5448ffceea0e','projects_project_tasks','Project','project','id','ProjectTask','project_task','project_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5063a4ef-2a53-32df-9725-5448ff67b0a8','project_tasks_created_by','Users','users','id','ProjectTask','project_task','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('522b9908-8855-6f59-b6ec-5448ff2b98ab','case_calls','Cases','cases','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('58475163-d695-c8e2-3a43-5448ffa733de','case_tasks','Cases','cases','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('58570403-4e1c-3eb4-845b-5448ff38d995','bug_emails','Bugs','bugs','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('5a7f86fd-bcae-d2d2-db8b-5448ffad8964','projects_assigned_user','Users','users','id','Project','project','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5bf7af13-d306-8a46-6604-5448ff932aae','leads_modified_user','Users','users','id','Leads','leads','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5e67e174-d152-69ec-92ed-5448ffa6d57d','case_notes','Cases','cases','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('63f60ec0-2681-bee2-22f9-5449006a0ec5','emails_users_rel','Emails','emails','id','Users','users','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Users',0,0),('647dc9ea-4ccf-4a98-67c0-544900e8550f','email_marketing_prospect_lists','EmailMarketing','email_marketing','id','ProspectLists','prospect_lists','id','email_marketing_prospect_lists','email_marketing_id','prospect_list_id','many-to-many',NULL,NULL,0,0),('6488bc89-dd64-54f1-d1fa-5448ff2f22a5','case_meetings','Cases','cases','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('67c867a3-d710-c298-3cd4-5448ffa76f43','leads_created_by','Users','users','id','Leads','leads','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('68b74ec1-74a5-6db9-25c1-5448ff918a16','projects_modified_user','Users','users','id','Project','project','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6a25a624-85b5-f389-bd0a-544900aa4cd0','documents_assigned_user','Users','users','id','Documents','documents','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6a565adc-defb-7d8b-dd05-5448ff300694','campaignlog_contact','CampaignLog','campaign_log','related_id','Contacts','contacts','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6aa664d9-75b1-b7d1-b163-5448ff487c67','case_emails','Cases','cases','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('6ad9775f-0680-0cea-8a7b-5448ff94f39b','prospectlists_assigned_user','Users','users','id','prospectlists','prospect_lists','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6d18cc71-ab8b-d664-b421-54490040cbd6','prospect_list_campaigns','ProspectLists','prospect_lists','id','Campaigns','campaigns','id','prospect_list_campaigns','prospect_list_id','campaign_id','many-to-many',NULL,NULL,0,0),('6edbea78-72c3-4d29-02b6-5448fff3714d','projects_created_by','Users','users','id','Project','project','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('71ff9e4e-1d5b-d534-c88d-5448ffb5115f','leads_assigned_user','Users','users','id','Leads','leads','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('72f10001-5588-8474-dde8-544900f82ee0','document_revisions','Documents','documents','id','DocumentRevisions','document_revisions','document_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('73ecfddc-19e4-70e7-e046-5448ffd6ccbd','campaignlog_lead','CampaignLog','campaign_log','related_id','Leads','leads','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('746332f1-d070-dff2-9ec7-5449006c3433','calls_users','Calls','calls','id','Users','users','id','calls_users','call_id','user_id','many-to-many',NULL,NULL,0,0),('7769a65b-b94b-1323-2b45-544900e9f9c8','opportunities_contacts','Opportunities','opportunities','id','Contacts','contacts','id','opportunities_contacts','opportunity_id','contact_id','many-to-many',NULL,NULL,0,0),('781b0dec-7581-03ac-a6de-5448ffadd512','leads_email_addresses','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Leads',0,0),('7c181ca5-7243-40de-4b36-54490059a26b','documents_accounts','Documents','documents','id','Accounts','accounts','id','documents_accounts','document_id','account_id','many-to-many',NULL,NULL,0,0),('7ca10195-111a-534c-e3d8-544900b4482d','calls_created_by','Users','users','id','Calls','calls','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7cf505be-d36a-6ab9-5ecd-5448ff8a9d16','contact_notes','Contacts','contacts','id','Notes','notes','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7e3aa80f-117b-2246-1a24-5448ff5798b6','leads_email_addresses_primary','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('80966696-a4b9-c2cc-cbab-5448ffe6d821','campaignlog_created_opportunities','CampaignLog','campaign_log','related_id','Opportunities','opportunities','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('81917813-43e4-722e-8e2e-54490094ea62','prospect_list_contacts','ProspectLists','prospect_lists','id','Contacts','contacts','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Contacts',0,0),('8458a59b-9f5e-c87b-e9d2-5448ffba568b','lead_direct_reports','Leads','leads','id','Leads','leads','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('87b1d8b6-ff12-b32f-128c-54490002f560','prospect_list_prospects','ProspectLists','prospect_lists','id','Prospects','prospects','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Prospects',0,0),('8a3f0df8-9eae-8317-2d61-5448ff8f4f71','campaign_campaignlog','Campaigns','campaigns','id','CampaignLog','campaign_log','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8a851cd8-dd5c-4f84-1ba9-5448ff1a9814','lead_tasks','Leads','leads','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('8dc6af78-22ae-c1c9-9462-5449002520c3','prospect_list_leads','ProspectLists','prospect_lists','id','Leads','leads','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Leads',0,0),('8e7986ca-13ce-ae60-8542-5448ff8d9182','campaignlog_targeted_users','CampaignLog','campaign_log','target_id','Users','users','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8ee3faf8-cc66-c575-be59-544900915eba','acl_roles_users','ACLRoles','acl_roles','id','Users','users','id','acl_roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),('8fc5cc23-cfc6-5810-a640-54490049b1a0','revisions_created_by','Users','users','id','DocumentRevisions','document_revisions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8fc66835-a0f7-2495-74d3-5448ff964912','schedulers_created_by_rel','Users','users','id','Schedulers','schedulers','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('90a84415-81d4-6623-2f8c-5448ff279316','lead_notes','Leads','leads','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('93f3cf52-eb17-337d-9004-544900833317','prospect_list_users','ProspectLists','prospect_lists','id','Users','users','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Users',0,0),('94863863-d53f-3530-d454-5448ffef885b','email_template_email_marketings','EmailTemplates','email_templates','id','EmailMarketing','email_marketing','template_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('960d3b79-329d-27d4-0bf4-5448ff5febda','notes_assigned_user','Users','users','id','Notes','notes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('97a95fab-11b6-13d3-43b6-5448ffaa0888','emailtemplates_assigned_user','Users','users','id','EmailTemplates','email_templates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('98ba5c52-a180-acbf-a378-544900db9e69','documents_bugs','Documents','documents','id','Bugs','bugs','id','documents_bugs','document_id','bug_id','many-to-many',NULL,NULL,0,0),('9abb3ab5-86f4-6d77-51d2-5448ff71a103','campaignlog_sent_emails','CampaignLog','campaign_log','related_id','Emails','emails','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9b1f39b9-7990-db52-d07f-5448ffd75847','schedulers_modified_user_id_rel','Users','users','id','Schedulers','schedulers','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9b600e37-6300-7d5f-228f-54490009773f','emails_meetings_rel','Emails','emails','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9bc80a9a-569e-9de6-ca81-54490015b336','meetings_modified_user','Users','users','id','Meetings','meetings','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9c2abff3-e2ce-29a3-ac7c-5449001149bf','prospect_list_accounts','ProspectLists','prospect_lists','id','Accounts','accounts','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Accounts',0,0),('9cb64bcb-e48b-cbfd-f690-54490037df37','accounts_contacts','Accounts','accounts','id','Contacts','contacts','id','accounts_contacts','account_id','contact_id','many-to-many',NULL,NULL,0,0),('9ce7fc17-f601-bda0-f7f4-5448fffe1598','lead_meetings','Leads','leads','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('9f12ba12-c8b9-8538-b964-5448ff2a4aba','notes_modified_user','Users','users','id','Notes','notes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a2969dcc-cf41-c4c3-3884-54490061eaab','leads_documents','Leads','leads','id','Documents','documents','id','linked_documents','parent_id','document_id','many-to-many','parent_type','Leads',0,0),('a4d6589e-bfaf-29d6-f810-5449007d7d8c','meetings_created_by','Users','users','id','Meetings','meetings','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a50f39f0-fe75-6324-36f3-5448ff07ffe5','lead_calls','Leads','leads','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('a6325588-8acc-d600-8e24-5448fffedfc7','notes_created_by','Users','users','id','Notes','notes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('abcc97b7-96ea-e5b5-5a0e-544900210fb5','meetings_assigned_user','Users','users','id','Meetings','meetings','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ac87e45d-2c8a-5d58-68b2-5448ffddebf8','opportunities_modified_user','Users','users','id','Opportunities','opportunities','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ae8e3c4e-6634-7e71-93b0-544900947719','inbound_email_created_by','Users','users','id','InboundEmail','inbound_email','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('af42967b-63ac-02a9-f205-5448fff55879','lead_emails','Leads','leads','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('b0befd5f-c2bd-acc5-acdd-5448ffb682d8','schedulers_jobs_rel','Schedulers','schedulers','id','SchedulersJobs','job_queue','scheduler_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b13d5620-2bb2-f84a-81c7-544900e2df3c','documents_cases','Documents','documents','id','Cases','cases','id','documents_cases','document_id','case_id','many-to-many',NULL,NULL,0,0),('b3144476-ee09-0deb-2482-544900980b92','meetings_notes','Meetings','meetings','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Meetings',0,0),('b38dd2e0-cdfc-bcd6-9579-544900c9a2fb','sugarfeed_modified_user','Users','users','id','SugarFeed','sugarfeed','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b46f70c0-b818-b99e-1ca8-5448ff31bf62','opportunities_created_by','Users','users','id','Opportunities','opportunities','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b6ac68c0-de63-2172-470f-544900e88bfa','emails_assigned_user','Users','users','id','Emails','emails','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b91846d9-bc5a-fc00-b055-5449000ee8a0','meetings_contacts','Meetings','meetings','id','Contacts','contacts','id','meetings_contacts','meeting_id','contact_id','many-to-many',NULL,NULL,0,0),('b965010c-90e5-f8d0-d9de-5448ffe1cb68','lead_campaign_log','Leads','leads','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Leads',0,0),('b9757b62-224b-f446-e401-544900d1b3af','sugarfeed_created_by','Users','users','id','SugarFeed','sugarfeed','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b9b82921-269d-84b8-784e-5448ff6d9204','campaigns_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ba836ee4-b2d6-ea94-d7e1-5448fffcf7db','opportunities_assigned_user','Users','users','id','Opportunities','opportunities','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bb0f0f3d-4c39-4608-062b-5448ff338b99','member_accounts','Accounts','accounts','id','Accounts','accounts','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bca4eae9-c5a7-5a43-c5fb-544900e27e8d','emails_bugs_rel','Emails','emails','id','Bugs','bugs','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Bugs',0,0),('c06be03b-aa50-4788-7c26-5448ff5201dc','contacts_modified_user','Users','users','id','Contacts','contacts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c0a3509c-5c10-60f9-9e22-5448ff1852e4','opportunity_calls','Opportunities','opportunities','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('c2feea5c-22a7-55e2-3720-5449006593a2','roles_users','Roles','roles','id','Users','users','id','roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),('c31b16b1-95e5-cb95-6a63-5448ff16b741','campaigns_created_by','Users','users','id','Campaigns','campaigns','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c600832b-d9ab-e928-70ce-54490024aa6d','projects_accounts','Project','project','id','Accounts','accounts','id','projects_accounts','project_id','account_id','many-to-many',NULL,NULL,0,0),('c6c7fad9-4e44-dc01-f7fe-5448ffd0a90b','opportunity_meetings','Opportunities','opportunities','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('c8db01e4-7104-ef7d-10ad-5449001006b1','acl_roles_actions','ACLRoles','acl_roles','id','ACLActions','acl_actions','id','acl_roles_actions','role_id','action_id','many-to-many',NULL,NULL,0,0),('c9c12597-7e7d-9894-fa94-5448ff31fb2a','campaigns_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ca64946e-947c-bc8b-a3c8-5449002714fc','emails_cases_rel','Emails','emails','id','Cases','cases','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Cases',0,0),('ca9cb6b2-1636-1932-a71b-544900ef938c','documents_modified_user','Users','users','id','Documents','documents','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cce12fd5-5679-6845-0d3d-5448ff8f0174','opportunity_tasks','Opportunities','opportunities','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('cf17c0b5-1493-deba-16c7-5448ff1bdcc4','contacts_created_by','Users','users','id','Contacts','contacts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cfdf8cfe-3840-f5e7-9abc-544900479418','documents_contacts','Documents','documents','id','Contacts','contacts','id','documents_contacts','document_id','contact_id','many-to-many',NULL,NULL,0,0),('cfe215a1-f618-204e-93da-5448ff452068','campaign_accounts','Campaigns','campaigns','id','Accounts','accounts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d11bda89-a8d8-e550-dc22-5448ffd57d9d','bugs_modified_user','Users','users','id','Bugs','bugs','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d303e056-c1ca-73dc-387a-5448ffb94525','opportunity_notes','Opportunities','opportunities','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('d3a0b15e-7bd6-44ec-0dbc-544900a7529e','meetings_leads','Meetings','meetings','id','Leads','leads','id','meetings_leads','meeting_id','lead_id','many-to-many',NULL,NULL,0,0),('d4bdba5d-375d-31aa-8469-544900789eb5','emails_modified_user','Users','users','id','Emails','emails','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d4e6646b-4d23-c53a-c5f2-5448ff29f081','schedulersjobs_assigned_user','Users','users','id','SchedulersJobs','schedulersjobs','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d66c4c78-d97e-aa05-ffcb-5448ff067622','contacts_assigned_user','Users','users','id','Contacts','contacts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d80bd8c8-ac78-6aa9-73ff-5448ff23ffe2','campaign_contacts','Campaigns','campaigns','id','Contacts','contacts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d89b8729-ae59-3bf4-e9d3-5448ffc19840','prospects_modified_user','Users','users','id','Prospects','prospects','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d8ff7029-1f47-5e1d-c4e1-5448fff37ec8','bugs_created_by','Users','users','id','Bugs','bugs','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d923c558-41e4-620a-1405-5448ffd6d06b','opportunity_emails','Opportunities','opportunities','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('d990095a-20f8-7930-55e9-5448ffd90511','contact_leads','Contacts','contacts','id','Leads','leads','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('da6e273c-c872-c245-072f-544900bde6f0','projects_bugs','Project','project','id','Bugs','bugs','id','projects_bugs','project_id','bug_id','many-to-many',NULL,NULL,0,0),('db99d7d4-dbb4-4343-c823-544900c9783e','emails_opportunities_rel','Emails','emails','id','Opportunities','opportunities','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Opportunities',0,0),('dc62fe99-8c3c-233a-380e-5448ff42cc94','accounts_modified_user','Users','users','id','Accounts','accounts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('dd2afa61-9111-e904-d5a0-544900aa1408','cases_bugs','Cases','cases','id','Bugs','bugs','id','cases_bugs','case_id','bug_id','many-to-many',NULL,NULL,0,0),('dd594948-2042-b199-1115-5448ffca3c2b','contacts_email_addresses','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Contacts',0,0),('de27456e-95de-0243-399c-5448ff3699d9','campaign_leads','Campaigns','campaigns','id','Leads','leads','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('de3c820b-b038-e6f8-ec50-544900225ee6','emails_created_by','Users','users','id','Emails','emails','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('de700280-8c7b-1920-a271-544900c2f5c6','calls_assigned_user','Users','users','id','Calls','calls','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('df20b0de-3239-ab1b-b028-5448ffe92a3c','bugs_assigned_user','Users','users','id','Bugs','bugs','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('df456b5c-7f89-ab67-91f0-5448ffedc0d4','opportunity_leads','Opportunities','opportunities','id','Leads','leads','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e0f59b97-0302-e5b6-d81d-544900c07684','eapm_modified_user','Users','users','id','EAPM','eapm','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e226fdae-d473-20b7-abec-5448fff68cd3','user_direct_reports','Users','users','id','Users','users','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e26d11a0-8083-f5f9-93dc-5448ffbf8cd3','accounts_created_by','Users','users','id','Accounts','accounts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e2806702-1cfb-99ca-ebe0-5448ff27934b','prospects_created_by','Users','users','id','Prospects','prospects','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e28b9188-d143-76f3-7c4d-5448ffd55bce','campaign_campaigntrakers','Campaigns','campaigns','id','CampaignTrackers','campaign_trkrs','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e4481408-fb85-f5a1-3f3a-5448ff0aa899','campaign_prospects','Campaigns','campaigns','id','Prospects','prospects','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e4ad3472-488b-a0c8-be4b-5448ff6b8744','contacts_email_addresses_primary','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('e5426dc4-1fb9-fe2a-2608-5448ff7f14c2','bug_tasks','Bugs','bugs','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('e55ab003-8c78-f578-ad89-5449008e728d','emails_notes_rel','Emails','emails','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e772ed53-8dfd-9358-9e33-5448ff0cbff9','opportunity_currencies','Opportunities','opportunities','currency_id','Currencies','currencies','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e7e3d60a-6be6-4d10-74e1-54490032f7d9','inbound_email_modified_user_id','Users','users','id','InboundEmail','inbound_email','modified_user_id',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('e85f4aa6-91d3-5d1f-787d-544900a5ca87','documents_opportunities','Documents','documents','id','Opportunities','opportunities','id','documents_opportunities','document_id','opportunity_id','many-to-many',NULL,NULL,0,0),('e940f51b-b872-b5b0-b5c1-5448ff85a46f','prospects_assigned_user','Users','users','id','Prospects','prospects','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e96d8cef-e3ca-7867-9f83-5448fff556d2','accounts_assigned_user','Users','users','id','Accounts','accounts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ea66ae35-fd7e-ae8e-82d7-5449000513aa','emails_tasks_rel','Emails','emails','id','Tasks','tasks','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Tasks',0,0),('ea6a1963-1231-b7b7-406f-5448ff289b2d','campaign_opportunities','Campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('eaf67bb6-30bc-8cf5-678f-544900832b77','eapm_created_by','Users','users','id','EAPM','eapm','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('eb6009f6-a491-85ed-7b65-5448ffa773aa','bug_meetings','Bugs','bugs','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('eb7edc94-b9a2-e16d-337c-544900dced6c','emails_contacts_rel','Emails','emails','id','Contacts','contacts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Contacts',0,0),('ec5c01e5-fb17-f08d-b908-5448ffc7b33f','campaign_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('edacd9d2-8a0e-ad1e-4025-5448ffe95a82','contact_direct_reports','Contacts','contacts','id','Contacts','contacts','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ee350e25-62aa-7ec8-8b1d-5448ffa47dab','prospect_tasks','Prospects','prospects','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('eea0db20-9e6b-2ca7-696e-5448ffeaeda7','opportunities_campaign','Campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f08bbbe4-080d-2545-c1f7-5448ff84a0a6','campaign_email_marketing','Campaigns','campaigns','id','EmailMarketing','email_marketing','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f0b6be3c-3b36-2e38-f403-5448ff2255b5','accounts_email_addresses','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Accounts',0,0),('f0c64000-9ab6-6bca-f943-5448ff9e6953','prospects_email_addresses','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Prospects',0,0),('f116d1ff-cd00-8b49-6b27-544900cacfdd','eapm_assigned_user','Users','users','id','EAPM','eapm','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f19c9089-40f0-2026-c843-544900d01b24','emails_accounts_rel','Emails','emails','id','Accounts','accounts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Accounts',0,0),('f2e175a2-5910-c097-437a-544900705a2d','calls_leads','Calls','calls','id','Leads','leads','id','calls_leads','call_id','lead_id','many-to-many',NULL,NULL,0,0),('f3877116-def6-4af4-b777-5448ff23e515','bug_calls','Bugs','bugs','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('f400b296-9658-ef37-bc16-5448ff7cceb6','calls_modified_user','Users','users','id','Calls','calls','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f41ea5a5-3d74-8bf5-4b20-54490033e176','meetings_users','Meetings','meetings','id','Users','users','id','meetings_users','meeting_id','user_id','many-to-many',NULL,NULL,0,0);
/*!40000 ALTER TABLE `relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releases`
--

DROP TABLE IF EXISTS `releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releases` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_order` int(4) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_releases` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releases`
--

LOCK TABLES `releases` WRITE;
/*!40000 ALTER TABLE `releases` DISABLE KEYS */;
/*!40000 ALTER TABLE `releases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `modules` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_role_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_modules`
--

DROP TABLE IF EXISTS `roles_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_modules` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `module_id` varchar(36) DEFAULT NULL,
  `allow` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_module_id` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_modules`
--

LOCK TABLES `roles_modules` WRITE;
/*!40000 ALTER TABLE `roles_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_users`
--

DROP TABLE IF EXISTS `roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ru_role_id` (`role_id`),
  KEY `idx_ru_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_users`
--

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved_search`
--

DROP TABLE IF EXISTS `saved_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_search` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `search_module` varchar(150) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` text,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `idx_desc` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_search`
--

LOCK TABLES `saved_search` WRITE;
/*!40000 ALTER TABLE `saved_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedulers`
--

DROP TABLE IF EXISTS `schedulers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulers` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `date_time_start` datetime DEFAULT NULL,
  `date_time_end` datetime DEFAULT NULL,
  `job_interval` varchar(100) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `catch_up` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_schedule` (`date_time_start`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedulers`
--

LOCK TABLES `schedulers` WRITE;
/*!40000 ALTER TABLE `schedulers` DISABLE KEYS */;
INSERT INTO `schedulers` VALUES ('10edb7f8-1263-4748-1b57-544900ab339e',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Prune SugarFeed Tables','function::trimSugarFeeds','2005-01-01 18:15:01','2020-12-31 23:59:59','0::2::1::*::*',NULL,NULL,NULL,'Active',1),('255a0e78-4717-9071-cd6a-54490092157f',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Clean Jobs Queue','function::cleanJobQueue','2012-01-01 19:45:01','2030-12-31 23:59:59','0::5::*::*::*',NULL,NULL,NULL,'Active',0),('874100af-fe41-a154-f638-5449008e4bce',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Removal of documents from filesystem','function::removeDocumentsFromFS','2012-01-01 14:45:01','2030-12-31 23:59:59','0::3::1::*::*',NULL,NULL,NULL,'Active',0),('9d1d89fc-d009-940a-fce4-544900355180',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Prune Tracker Tables','function::trimTracker','2005-01-01 11:15:01','2020-12-31 23:59:59','0::2::1::*::*',NULL,NULL,NULL,'Active',1),('b5e4d5ed-dac1-ca6c-adb2-544900b854e0',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Check Inbound Mailboxes','function::pollMonitoredInboxes','2005-01-01 07:15:01','2020-12-31 23:59:59','*::*::*::*::*',NULL,NULL,'2014-10-27 22:20:01','Active',0),('cdc75f4a-15e1-ecc7-f483-544900af2037',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Run Nightly Process Bounced Campaign Emails','function::pollMonitoredInboxesForBouncedCampaignEmails','2005-01-01 19:45:01','2020-12-31 23:59:59','0::2-6::*::*::*',NULL,NULL,'2014-10-23 13:36:03','Active',1),('e0162c62-d498-5e4e-6218-544900046e1b',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Run Nightly Mass Email Campaigns','function::runMassEmailCampaign','2005-01-01 17:00:01','2020-12-31 23:59:59','0::2-6::*::*::*',NULL,NULL,'2014-10-23 13:36:03','Active',1),('e62a86ae-0bf6-a80d-e6ee-544900a41a96',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Prune Database on 1st of Month','function::pruneDatabase','2005-01-01 15:30:01','2020-12-31 23:59:59','0::4::1::*::*',NULL,NULL,NULL,'Inactive',0),('ee51b51d-842a-7028-cc7b-5449003170ee',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Run Email Reminder Notifications','function::sendEmailReminders','2008-01-01 10:30:01','2020-12-31 23:59:59','*::*::*::*::*',NULL,NULL,'2014-10-27 22:20:01','Active',0);
/*!40000 ALTER TABLE `schedulers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sugarfeed`
--

DROP TABLE IF EXISTS `sugarfeed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sugarfeed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `related_module` varchar(100) DEFAULT NULL,
  `related_id` char(36) DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sgrfeed_date` (`date_entered`,`deleted`),
  KEY `idx_sgrfeed_rmod_rid_date` (`related_module`,`related_id`,`date_entered`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sugarfeed`
--

LOCK TABLES `sugarfeed` WRITE;
/*!40000 ALTER TABLE `sugarfeed` DISABLE KEYS */;
/*!40000 ALTER TABLE `sugarfeed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `date_due_flag` tinyint(1) DEFAULT '0',
  `date_due` datetime DEFAULT NULL,
  `date_start_flag` tinyint(1) DEFAULT '0',
  `date_start` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tsk_name` (`name`),
  KEY `idx_task_con_del` (`contact_id`,`deleted`),
  KEY `idx_task_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_task_assigned` (`assigned_user_id`),
  KEY `idx_task_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracker`
--

DROP TABLE IF EXISTS `tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitor_id` char(36) NOT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` varchar(36) DEFAULT NULL,
  `item_summary` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `session_id` varchar(36) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_tracker_iid` (`item_id`),
  KEY `idx_tracker_userid_vis_id` (`user_id`,`visible`,`id`),
  KEY `idx_tracker_userid_itemid_vis` (`user_id`,`item_id`,`visible`),
  KEY `idx_tracker_monitor_id` (`monitor_id`),
  KEY `idx_tracker_date_modified` (`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracker`
--

LOCK TABLES `tracker` WRITE;
/*!40000 ALTER TABLE `tracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upgrade_history`
--

DROP TABLE IF EXISTS `upgrade_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upgrade_history` (
  `id` char(36) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `md5sum` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `version` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `id_name` varchar(255) DEFAULT NULL,
  `manifest` longtext,
  `date_entered` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `upgrade_history_md5_uk` (`md5sum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upgrade_history`
--

LOCK TABLES `upgrade_history` WRITE;
/*!40000 ALTER TABLE `upgrade_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `upgrade_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_preferences`
--

DROP TABLE IF EXISTS `user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preferences` (
  `id` char(36) NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` longtext,
  PRIMARY KEY (`id`),
  KEY `idx_userprefnamecat` (`assigned_user_id`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_preferences`
--

LOCK TABLES `user_preferences` WRITE;
/*!40000 ALTER TABLE `user_preferences` DISABLE KEYS */;
INSERT INTO `user_preferences` VALUES ('86bd36b1-a0d1-5c4c-edff-54490095dbee','global',0,'2014-10-23 13:17:35','2014-10-23 13:28:04','1','YToyNzp7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MzY6Ijg2YTFiNTlmLTg2ODEtNWIzMy1iODljLTU0NDkwMDE4YzRiZiI7czoxMDoidXNlcl90aGVtZSI7czo2OiJTdWdhcjUiO3M6MTM6InJlbWluZGVyX3RpbWUiO2k6MTgwMDtzOjEyOiJtYWlsbWVyZ2Vfb24iO3M6Mjoib24iO3M6ODoidGltZXpvbmUiO3M6MTE6IkV1cm9wZS9SaWdhIjtzOjE2OiJzd2FwX2xhc3Rfdmlld2VkIjtzOjA6IiI7czoxNDoic3dhcF9zaG9ydGN1dHMiO3M6MDoiIjtzOjE5OiJuYXZpZ2F0aW9uX3BhcmFkaWdtIjtzOjI6ImdtIjtzOjEzOiJzdWJwYW5lbF90YWJzIjtzOjA6IiI7czoxNDoibW9kdWxlX2Zhdmljb24iO3M6MDoiIjtzOjk6ImhpZGVfdGFicyI7YTowOnt9czoxMToicmVtb3ZlX3RhYnMiO2E6MDp7fXM6Nzoibm9fb3BwcyI7czozOiJvZmYiO3M6MTk6ImVtYWlsX3JlbWluZGVyX3RpbWUiO2k6LTE7czoyOiJ1dCI7czoxOiIxIjtzOjg6ImN1cnJlbmN5IjtzOjM6Ii05OSI7czozNToiZGVmYXVsdF9jdXJyZW5jeV9zaWduaWZpY2FudF9kaWdpdHMiO3M6MToiMiI7czoxMToibnVtX2dycF9zZXAiO3M6MToiLCI7czo3OiJkZWNfc2VwIjtzOjE6Ii4iO3M6NToiZGF0ZWYiO3M6NToiZC5tLlkiO3M6NToidGltZWYiO3M6MzoiSDppIjtzOjI2OiJkZWZhdWx0X2xvY2FsZV9uYW1lX2Zvcm1hdCI7czo1OiJzIGYgbCI7czoxNDoidXNlX3JlYWxfbmFtZXMiO3M6Mjoib24iO3M6MTc6Im1haWxfc210cGF1dGhfcmVxIjtzOjA6IiI7czoxMjoibWFpbF9zbXRwc3NsIjtpOjA7czoxNzoiZW1haWxfc2hvd19jb3VudHMiO2k6MDtzOjE5OiJ0aGVtZV9jdXJyZW50X2dyb3VwIjtzOjM6IkFsbCI7fQ=='),('ac550ad6-1f7a-6308-a778-544902d06374','Accounts2_ACCOUNT',0,'2014-10-23 13:28:09','2014-10-23 13:28:09','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('f0adc6bc-d0e5-1f8e-2f4a-5449027406ef','ETag',0,'2014-10-23 13:28:04','2014-10-23 13:28:04','1','YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6MTt9');
/*!40000 ALTER TABLE `user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `user_name` varchar(60) DEFAULT NULL,
  `user_hash` varchar(255) DEFAULT NULL,
  `system_generated_password` tinyint(1) DEFAULT NULL,
  `pwd_last_changed` datetime DEFAULT NULL,
  `authenticate_id` varchar(100) DEFAULT NULL,
  `sugar_login` tinyint(1) DEFAULT '1',
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `external_auth_only` tinyint(1) DEFAULT '0',
  `receive_notifications` tinyint(1) DEFAULT '1',
  `description` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `phone_home` varchar(50) DEFAULT NULL,
  `phone_mobile` varchar(50) DEFAULT NULL,
  `phone_work` varchar(50) DEFAULT NULL,
  `phone_other` varchar(50) DEFAULT NULL,
  `phone_fax` varchar(50) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `address_street` varchar(150) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `portal_only` tinyint(1) DEFAULT '0',
  `show_on_employees` tinyint(1) DEFAULT '1',
  `employee_status` varchar(100) DEFAULT NULL,
  `messenger_id` varchar(100) DEFAULT NULL,
  `messenger_type` varchar(100) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `is_group` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_name` (`user_name`,`is_group`,`status`,`last_name`,`first_name`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('1','admin','$6$SWZT4vW2NQdy$IYthqkV1ufZY7jbYtkxOq4p5s2a6rI5/UQZEmtLFntWqMt7UQu2QhlCgyiDKPt4A5Zy/lObUwiJzZQGKx4rSG0',0,NULL,NULL,1,NULL,'Administrator',1,0,1,NULL,'2014-10-23 13:17:35','2014-10-23 13:28:04','1','','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,0,0,1,'Active',NULL,NULL,'',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_feeds`
--

DROP TABLE IF EXISTS `users_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_feeds` (
  `user_id` varchar(36) DEFAULT NULL,
  `feed_id` varchar(36) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_ud_user_id` (`user_id`,`feed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_feeds`
--

LOCK TABLES `users_feeds` WRITE;
/*!40000 ALTER TABLE `users_feeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_last_import`
--

DROP TABLE IF EXISTS `users_last_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_last_import` (
  `id` char(36) NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `import_module` varchar(36) DEFAULT NULL,
  `bean_type` varchar(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_last_import`
--

LOCK TABLES `users_last_import` WRITE;
/*!40000 ALTER TABLE `users_last_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_last_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_password_link`
--

DROP TABLE IF EXISTS `users_password_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_password_link` (
  `id` char(36) NOT NULL,
  `username` varchar(36) DEFAULT NULL,
  `date_generated` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_password_link`
--

LOCK TABLES `users_password_link` WRITE;
/*!40000 ALTER TABLE `users_password_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_password_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_signatures`
--

DROP TABLE IF EXISTS `users_signatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_signatures` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `signature` text,
  `signature_html` text,
  PRIMARY KEY (`id`),
  KEY `idx_usersig_uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_signatures`
--

LOCK TABLES `users_signatures` WRITE;
/*!40000 ALTER TABLE `users_signatures` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_signatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vcals`
--

DROP TABLE IF EXISTS `vcals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vcals` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `idx_vcal` (`type`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vcals`
--

LOCK TABLES `vcals` WRITE;
/*!40000 ALTER TABLE `vcals` DISABLE KEYS */;
/*!40000 ALTER TABLE `vcals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versions`
--

DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_version` varchar(255) DEFAULT NULL,
  `db_version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_version` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versions`
--

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` VALUES ('55350ec1-f869-10cb-ca9b-5449007d5c15',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Chart Data Cache','3.5.1','3.5.1'),('6073b7bc-3b40-04a5-ac47-5449001a6da3',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','htaccess','3.5.1','3.5.1'),('66f97c19-795b-3b7c-a03e-54490049744a',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Rebuild Relationships','4.0.0','4.0.0'),('6d1615cc-0604-246e-55e2-544900a20407',0,'2014-10-23 13:17:35','2014-10-23 13:17:35','1','1','Rebuild Extensions','4.0.0','4.0.0');
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-28  0:30:35
