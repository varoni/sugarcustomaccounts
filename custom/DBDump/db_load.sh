#!/bin/sh
# Atjaunot, nedzesot datubazi, dropojot tikai izveidotas tabulas.
mysqldump -u`cat ../../.crmdbusername` -p`cat ../../.crmdbpassword` --no-data --add-drop-table `cat ../../.crmdatabase` | grep ^DROP | mysql -u`cat ../../.crmdbusername` -p`cat ../../.crmdbpassword` `cat ../../.crmdatabase`
cat db.sql | mysql -u`cat ../../.crmdbusername` -p`cat ../../.crmdbpassword` `cat ../../.crmdatabase`
